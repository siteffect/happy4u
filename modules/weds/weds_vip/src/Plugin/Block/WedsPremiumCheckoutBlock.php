<?php
/**
 * @file
 * Contains \Drupal\weds_vip\Plugin\Block\WedsPremiumCheckoutBlock.
 */

namespace Drupal\weds_vip\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\weds\Weds;
use Drupal\weds_vip\WedsVip;

/**
 *
 * @Block(
 *   id = "weds_premium_checkout",
 *   admin_label = @Translation("Weds Premium Checkout Block"),
 * )
 */
class WedsPremiumCheckoutBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();

    $ya = '<iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=Premium&targets-hint=&default-sum=2&button-text=11&hint=&successURL=&quickpay=shop&account=41001228728326" width="423" height="224" frameborder="0" allowtransparency="true" scrolling="no"></iframe>';
    
    $success_url = Url::fromUri('internal:/weds/ya/success')->setAbsolute($absolute = true)->toString();

    $block = [
       //'#children' => $ya,
       '#uid' => $uid,
       '#theme' => 'weds_vip_ya_block',
       '#success_url' => $success_url,
    ];
    //$block['#attached']['library'][] = 'weds_vip/weds-vip-checkout';
    $block['#attached']['library'][] = 'core/drupal.dialog.ajax';    
    return $block;
  }

}

