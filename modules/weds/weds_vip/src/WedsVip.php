<?php
/**
 * @file
 * Contains \Drupal\weds_vip\WedsVip.
 */

namespace Drupal\weds_vip;

use Drupal\user\Entity\User;
use Drupal\weds\Weds;
 
class WedsVip {
  

  public static function get_vip_blocks() {
      return self::vip_blocks_data();
  }
 


  // Временное решение для хранения данных
  public static function vip_fields_data(){
    return array(
      'field_groom_social',
      'field_bride_social',
    );
  }

  public static function vip_blocks_data(){
    return array(
      'weds_photoalbum_block',
      'weds_rsvp_block',
    );
  }
}