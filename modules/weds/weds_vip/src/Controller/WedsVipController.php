<?
 /**
  * @file
  * Contains \Drupal\weds_vip\Controller\WedsProtectedController.
  */

namespace Drupal\weds_vip\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\weds\Weds;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;


class WedsVipController extends ControllerBase {

  public function contentStartPremium() {
    $user = \Drupal::currentUser();
    if ( $user->id() == 0 ) {
      $url = '/weds/register?premium=premium';
      $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
      $response->send();
    }

    $output = array();
    $output['#title'] = 'Start Premium';

    $block = \Drupal::service('plugin.manager.block')
      ->createInstance('weds_premium_checkout')
      ->build();
    $output += $block;

    return $output;

  }


  public function contentGetVip() {
    $output = array();
    $output['#title'] = 'Premium';

    $block = \Drupal\block_content\Entity\BlockContent::load(8);
    $block_view = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block, 'DISPLAY_VIEW_NAME');

    $output += $block_view;

    return $output;
  }


  public function yaResponse() {
    $response = new Response();
    //$message = http_build_query($_POST,'',', ');

    $secret_key = 'q0djOiujWG5MKqkAbJ9B4ym4';
    $sha1 = sha1( $_POST['notification_type'] . '&'. $_POST['operation_id']. '&' . $_POST['amount'] . '&643&' . $_POST['datetime'] . '&'. $_POST['sender'] . '&' . $_POST['codepro'] . '&' . $secret_key. '&' . $_POST['label'] );
    //$message .= '  ' . $sha1;
    $message = 'User '. $_POST['label'] . 'got premium.';
    \Drupal::logger('weds_vip')->notice($message);

    if ($sha1 == $_POST['sha1_hash'] ) {
      if(!empty($uid = $_POST['label'])) {
        $account = \Drupal::entityTypeManager()
        ->getStorage('user')
        ->load($uid);
        $account->addRole('vip');
        $account->save();

        // Send Pulse ------------
        $reg_post_data = array(
          'email' => $account->mail->getValue()[0]['value'],
          'phone' => '555',
          'order_date' => date('Y-m-d'),
          'user_id' => $account->Id(),
          'groom_name' => $account->field_groom_name->getValue()[0]['value'],
          'bride_name' => $account->field_bride_name->getValue()[0]['value'],
        );

         $response_sendpulse = \Drupal::httpClient()->post('https://events.sendpulse.com/events/id/2c34d21500ad43d5ae07361da17c06e2', [
          'verify' => true,
          'form_params' => $reg_post_data,
            'headers' => [
              'Content-type' => 'application/x-www-form-urlencoded',
            ],
        ])->getBody()->getContents();  
        // -----------------------

      }
      
      $response->setStatusCode(200);
    }
    else {
      $response->setStatusCode(403);
    }
    return $response;
  }

  public function yaSuccess() {
    $user = \Drupal::currentUser();

    if ( in_array('vip', $user->getRoles() )) {
      if ( in_array('client', $user->getRoles() )) {
        $url = '/weds/'.$user->id().'/edit/';
      }
      if ( in_array('client_registry', $user->getRoles() )) {
        $url = '/weds/register';
      }
      drupal_set_message(t('<div id="start_premium">Поздравляем! Теперь вам доступны все возможности PREMIUM-аккаунта (подробнее о них можно прочитать <a href="/premium">здесь</a>).</div>'));
    }

    else {
      drupal_set_message(t('<div id="start_premium_error">Что-то не так с оплатой. Подождите некоторое время. Возможно ваш премиум-аккаунт будет активирован позже.</div>', 'error'));
    }


    $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
    $response->send();
    

    // $output = array();
    // $output['#title'] = 'ok';
    // $output['#markup'] = 'ok';
    // return $output;
   
   }  
  //q0djOiujWG5MKqkAbJ9B4ym4

}