(function ($) {
  Drupal.behaviors.weds_vip = {
    attach: function(context, drupalSettings) {
      //console.log(drupalSettings)
      function checkoutInit(url) {
		  $ipsp('checkout').scope(function() {
		    this.setCheckoutWrapper('#checkout_wrapper');
		    this.addCallback(__DEFAULTCALLBACK__);
		    this.action('show', function(data) {
		      $('#checkout_loader').remove();
		      $('#checkout').show();
		    });
		    this.action('hide', function(data) {
		      $('#checkout').hide();
		    });
		    this.action('resize', function(data) {
		      $('#checkout_wrapper').width(480).height(data.height);
		    });
		    this.loadUrl(url);

		  });
		};
		var button = $ipsp.get("button");

		button.setMerchantId(1404238);
		//button.setMerchantId(1396424);
		button.setAmount(300.00, 'RUB', true);
		button.setHost('api.fondy.eu');
		button.setProtocol("http");
		/*button.addRecurringData({
				"period": 'year',
				"every": 1
		});*/
		button.setRecurringReadonly(true); 
		button.setResponseUrl('http://happy4ever.ru/weds/fondy/response');
		button.addParam("lang","ru");
		button.addField({
		   "hidden": true,
		   "readonly": true,
		   "label": "uid",  
		   "name": "weds_client_uid",
		   "value": Drupal.settings.weds.uid
		});
		button.addField({
		   "hidden": true,
		   "readonly": true,
		   "label": "action", 
		   "name": "weds_action",
		   "value": Drupal.settings.weds.action
		});
		button.addField({
		   "hidden": true,
		   "readonly": true,
		   "label": "destination", 
		   "name": "weds_destination",
		   "value": Drupal.settings.weds.destination
		});

		//Math.round(Math.random()*1000000));
		checkoutInit(button.getUrl());
    }
  }

})(jQuery);
