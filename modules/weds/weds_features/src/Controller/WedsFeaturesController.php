<?
 /**
  * @file
  * Contains \Drupal\weds_features\Controller\WedsFeaturesController.
  */

namespace Drupal\weds_features\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\weds\Weds;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class WedsFeaturesController extends ControllerBase {

  public function contentSeating() {

      $output = array();
      $output['#title'] = 'Рассадка гостей по столам';

      $block = \Drupal::service('plugin.manager.block')
        ->createInstance('weds_seating_block')
        ->build();
      $output += $block;

      // $route_match = \Drupal::service('current_route_match');
      // $route_name = $route_match->getRouteName();
      //return ['#markup' => $route_name];

      // $params = json_decode($output, TRUE);
      // return new JsonResponse($params);

      return $output;
    
  }


}