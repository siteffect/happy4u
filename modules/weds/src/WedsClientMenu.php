<?php
/**
 * @file
 * Contains \Drupal\weds\WedsClientMenu.
 */

namespace Drupal\weds;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\weds\Weds;
 
class WedsClientMenu {

  public static function get_client_menu_list($uid) {
    $edit_data = Weds::weds_edit_data();
    $client_menu_list = Array(); 

    foreach ($edit_data as $name => $item){ 
      $menu_item = Array();
      $attributes = array(
        'class' => Array(),
      );
      $classes = Array();

      $href ='/weds/'. $uid . '/edit/' . $name;
      $url = Url::fromUri('internal:'.$href);

      $current_path = \Drupal::service('path.current')->getPath();
      if ($current_path  == $href) {
        $classes[] = 'active';
      }

      $link = Link::fromTextAndUrl($item['title'], $url);
      $link = $link->toRenderable();


      // Vip
      $user = \Drupal::entityTypeManager()
        ->getStorage('user')
        ->load($uid);
      $moduleHandler = \Drupal::service('module_handler');
      if ($moduleHandler->moduleExists('weds_vip')){
        if($item['vip']) {
         if ( !in_array('vip', $user->getRoles()) ){
          $classes[] = 'weds-client-menu-vip';
          $menu_item['vip'] = TRUE;

          $url = Url::fromUri('internal:/weds/get-vip');
          
          $golg_link_options = array(
            'attributes' => array(
              'class' => array(
                'use-ajax',
                'gold',
              ),
              'data-dialog-type' => array(
                'modal',
              ),
              'title' => 'UpGrade!',
            ),
          );
          $url->setOptions($golg_link_options);

          $golg_link = Link::fromTextAndUrl($item['title'], $url);

          $link = $golg_link->toRenderable();
         }
        }
        else {$menu_item['vip'] = FALSE;}
      }

        $attributes['title'] = $item['title'];
        $menu_item['class'] = implode(' ', $classes);
        $menu_item['link'] = $link;

        $link['#attributes'] = $attributes; 
        $client_menu_list[] = $menu_item;
    }
    return $client_menu_list;

  }

 
}