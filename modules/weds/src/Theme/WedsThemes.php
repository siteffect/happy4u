<?php

namespace Drupal\weds\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\user\Entity\User;
use Drupal\weds\Weds;

/**
 * Sets the Stark for frontpage.
 */
class WedsThemes implements ThemeNegotiatorInterface {

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * StarkForFront constructor.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   */
  public function __construct(PathMatcherInterface $pathMatcher) {
    $this->pathMatcher = $pathMatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $routeMatch) {
    if (Weds::is_wedding_site()) {
      $user = Weds::weds_user();
      if(!empty($user) && in_array('client', $user->getRoles())){
        if($user->field_theme->value)
          return true;
      }

      //$entity->get("field_groom_name")->getValue()
    }
    
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $routeMatch) {
    $user = Weds::weds_user();
    $theme = $user->field_theme->value;

    # Машинное имя темы, которую необходимо активировать.
    return $theme;
  }
}