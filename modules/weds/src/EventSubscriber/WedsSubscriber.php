<?php

namespace Drupal\weds\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;

class WedsSubscriber implements EventSubscriberInterface {

  public function checkForRedirection(GetResponseEvent $event) {
    //ksm($event->getRequest());

    $user = $event->getRequest()->get('user');
    if ($event->getRequest()->get('_route') == 'entity.user.canonical') {
      if(in_array('client_registry', $user->getRoles())){
        $url = '/weds/register';
        $event->setResponse(new RedirectResponse($url));
      }
      if(in_array('client', $user->getRoles())){
        $url = '/weds/'.$user->id().'/edit';
        $event->setResponse(new RedirectResponse($url));
      }
    }

    if ($event->getRequest()->get('_route') == 'user.register') {
      if(empty($user)) {
        $url = '/weds/register';
        $event->setResponse(new RedirectResponse($url));
      }
    }

    //redirect.destination
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    return $events;
  }

}
