<?php

/**
 * @file
 * Contains \Drupal\weds\Form\WedsRegisterFinishForm.
 */

namespace Drupal\weds\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\user\Entity\User;

class WedsRegisterFinishForm extends ContentEntityForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'weds_wizard';
  }


  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    self::setEntity(User::load($this->currentUser()->id()));

    $form = parent::buildForm($form, $form_state);

    $account = $this->entity;

    $form['actions']['submit']['#value'] = 'Закончить регистрацию и перейти на сайт!';
    // ksm($form);
    // ksm($form_state);
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $account = $this->entity;

    $account->field_weds_client_step->value = 100;
    $account->removeRole('client_registry');
    $account->addRole('client');
    $account->save();
    $form_state->set('user', $account);


     // Send Pulse ------------
    /*$reg_post_data = array(
      'email' => $account->mail->getValue()[0]['value'],
      'phone' => '555',
      'reg_date' =>  date('Y-m-d'),
      'user_id' => $account->Id(),
      'groom_name' => $account->field_groom_name['und'][0]['value'],
      'bride_name' => $account->field_bride_name['und'][0]['value'],
    );

    $response_sendpulse = \Drupal::httpClient()->post('https://events.sendpulse.com/events/id/7f0ff96e091440bbb29b636459e9eb86', [
      'verify' => true,
      'form_params' => $reg_post_data,
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
      ])->getBody()->getContents();*/  
    // -----------------------

    //$form_state->setRebuild();

    //$form_state->setRedirect('entity.user.edit_form');
  }
}