<?php

/**
 * @file
 * Contains \Drupal\weds\Form\WedsRegisterContinueForm.
 */

namespace Drupal\weds\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\AccountForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\weds\Weds;

class WedsEditForm extends AccountForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'weds_edit_form';
  }


  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $account = $this->entity;    
    $route_match = \Drupal::service('current_route_match');
    $edit_item = $route_match->getParameter('item');
    $form_state->set('edit_item', $edit_item);
    

    if($edit_item == 'weds_site') {
      $route_match = \Drupal::service('current_route_match');
      $uid = $route_match->getParameter('user')->Id();
      $theme_name = Weds::get_client_theme($uid);
      $theme_settings = Weds::get_theme_settings($uid, $theme_name);
      $theme_path = drupal_get_path('theme', $theme_name);
      $filename = $theme_path . '/theme-settings.php';

      if (file_exists($filename)) {
        require_once $filename;
        // The file must be required for the cached form too.
        $files = $form_state->getBuildInfo()['files'];
        if (!in_array($filename, $files)) {
          $files[] = $filename;
        }
        $form_state->addBuildInfo('files', $files);

        // Call theme-specific settings.
          $function = $theme_name . '_form_system_theme_settings_alter';
          if (function_exists($function)) {
            //$settings_form = $function([], $form_state);
            $function($form, $form_state);
            unset($form['weds_admin']);
            $form['weds_client']['#weight'] = 9;

            foreach($theme_settings as $prop => $value){
              // ksm($form_state->getValue($prop));
              $form['weds_client'][$prop]['#default_value'] = $theme_settings[$prop];
            }

          }

          //Выбираем настройки-файлы
          foreach ( $form['weds_client'] as $k => $element ){   
            if (isset($element['#type']) && $element['#type']=='managed_file'){
              $form_state->setValues(['theme_settings_files' => $k]);
            }
          }
      }
 
    } 

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // parent::save($form,$form_state);
    $account = $this->entity;
    $account->save();

    if (isset($form['weds_client'] )) {
      $uid = Weds::weds_user()->id();
      $theme = Weds::get_client_theme($uid);
      $theme_settings = Weds::get_theme_settings($uid, $theme);
        
        $theme_settings = Array();
        
          foreach ( $form['weds_client'] as $k => $element ){   
            if (substr($k, 0, 1) != '#' && isset($element['#type']) && $element['#type']!='hidden'){  
                $theme_setings_fields[]=$k;  
              if ( $form_state->getValue($k) ) {
                $theme_settings[$k] = $form_state->getValue($k);
              }
             }    
          }
       
        Weds::set_theme_settings($uid, $theme, $theme_settings);

        $theme_path = drupal_get_path('theme', $theme);
        $filename = $theme_path . '/theme-settings.php';

        if (file_exists($filename)) {
          require_once $filename;    

          // Call theme-specific settings.
          // Примемить фунцию, которая устанавливает сатус файла 1
          $function = '_' . $theme . '_form_system_theme_settings_submit';
          if (function_exists($function)) {
            $function($form, $form_state);
          }
       }               
     }

    if ( strpos(\Drupal::request()->query->get('destination'), 'weds/subdomain') !== false ) {
      $uid = Weds::weds_user()->id();
      $response = new TrustedRedirectResponse(Weds::get_wedding_site_url($uid));
      $response->send();
    }
     

  }


}