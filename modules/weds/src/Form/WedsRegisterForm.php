<?php

namespace Drupal\weds\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RegisterForm;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form handler for the user register forms.
 */
class WedsRegisterForm extends RegisterForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'weds_wizard';
  }


  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['account']['name']['#value']  = md5(date('c'));
    $form['account']['name']['#access'] = FALSE;

    /*$form['captcha'] = array(
      '#type' => 'captcha',
      '#captcha_type' => 'recaptcha/reCAPTCHA',
      '#weight' => 90,
    );*/
 
    //\Drupal::moduleHandler()->invoke('user_registrationpassword', $hook, $args = array())

     //ksm($form);
    //$form['#token'] = FALSE;
    $form['actions']['submit']['#value'] = $this->t('Next');
    return $form;
  }


    /*if ( empty($form_state['storage']['step']) ) {
    if ($user->uid){
      $account = user_load($user->uid);
      $form_state['storage']['step'] = $account->field_weds_client_step['und'][0]['value'];
    }
    else
      $form_state['storage']['step'] = 0;
    }
    */

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
 
    // $form_state->set('user', $account);
    // $form_state->setValue('uid', $account->id());

    $account = $this->entity;
    $account->setUsername($this->entity->getEmail());
    

    parent::save($form, $form_state);    

    //$account->field_weds_client_step->value = 1;
    $account->set('field_weds_client_step', 1);
    $account->addRole('client_registry');
    $account->save();
    //$form_state->set('user', $account);


    // Send Pulse ------------
     $reg_post_data = array(
      'email' => $account->mail->getValue()[0]['value'],
      'phone' => '555',
      'reg_date' => date('Y-m-d'),
      'user_id' => $account->Id(),
      'one_time_login_url' => user_registrationpassword_confirmation_url($account),
      'groom_name' => $account->field_groom_name->getValue()[0]['value'],
      'bride_name' => $account->field_bride_name->getValue()[0]['value'],
    );

     $response_sendpulse = \Drupal::httpClient()->post('https://events.sendpulse.com/events/id/fbce8194b54f4804bff1f6dfd8cccb6c', [
      'verify' => true,
      'form_params' => $reg_post_data,
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
    ])->getBody()->getContents();  
    // -----------------------


    if (\Drupal::moduleHandler()->moduleExists('user_registrationpassword')) {
      user_registrationpassword_form_user_register_submit($form, $form_state);
    }
    //user_login_finalize($account);
    $form_state->setRebuild();
    $response = new RedirectResponse('/weds/register'); // !KISS!
    $response->send();
    //$form_state->setRedirect('weds.register');
  }

} 