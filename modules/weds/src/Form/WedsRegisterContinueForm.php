<?php

/**
 * @file
 * Contains \Drupal\weds\Form\WedsRegisterContinueForm.
 */

namespace Drupal\weds\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class WedsRegisterContinueForm extends ContentEntityForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'weds_wizard';
  }


  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    self::setEntity(User::load($this->currentUser()->id()));

    /* @var $entity \Drupal\user\Entity\User */
    $form = parent::buildForm($form, $form_state);

    $account = $this->entity;    

    $step = $account->field_weds_client_step->value;

    /* Выбранная тема */
    if(isset($_GET['theme']) && $step == 1) {
      //$form['field_theme']['widget'][0]['value']['#default_value'] = $_GET['theme'];
      $account->set('field_theme', $_GET['theme']);
      $account->save();
    }
    
    $form['actions']['submit']['#value'] = $this->t('Next');
    

    // ksm($form);
    // ksm($form_state);
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    
    $account = $this->entity;

    $step = $account->field_weds_client_step->value;

    $step = (int)$step;
    
    $account->field_weds_client_step->value = $step+1;
    $account->save();

    $form_state->set('user', $account);

    $step = (int)$step;
    //$form_state->setValue('uid', $account->id());
    
    //$step = $form_state->get('step');
    //$form_state->set('step',  $step+1);

    //$form_state->setRebuild();
    return new RedirectResponse(\Drupal::url('<front>', [], ['absolute' => TRUE]));
    $form_state->setRedirect('weds.register');
    // $form_state->setRedirect(
    //   'entity.user.edit_form',
    //   ['user' => $this->entity->id()]
    // );
  }
}