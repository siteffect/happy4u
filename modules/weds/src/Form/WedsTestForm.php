<?php

/**
 * @file
 * Contains \Drupal\weds\Form\WedsTestForm.
 */

namespace Drupal\weds\Form;

 
use Drupal\user\Entity\User;
use Drupal\user\AccountForm;                 
use Drupal\Core\Form\FormStateInterface; 
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
 
/**
 * Configure example settings for this site.
 */
class WedsTestForm extends AccountForm  {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weds_test_form';
  }
 

 
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $account = $this->entity; 

    $form['test'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
    );



    $form['fieldname'] = array(
      '#type'          => 'managed_file',
      '#title'         => t('Choose your file to upload'),
      '#upload_location' => 'public://images/',
      '#default_value' => array(),
      '#description'   => t('Your description for upload file'),
      '#states'        => array(
        'visible'      => array(
          ':input[name="image_type"]' => array('value' => t('Upload your file(s)')),
        ),
      ),
    );

    $form['actions']['submit'] = [
     '#type' => 'submit',
     '#value' => $this->t('Отправить форму'),
    ];
 
    return $form;
  }
 
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    ksm($form_state->getValues());
 
    //parent::submitForm($form, $form_state);
  }
}