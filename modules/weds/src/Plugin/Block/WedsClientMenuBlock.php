<?php
/**
 * @file
 * Contains \Drupal\weds\Plugin\Block\WedsClientMenuBlock.
 */

namespace Drupal\weds\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\weds\Weds;
use Drupal\weds\WedsClientMenu;

/**
 *
 * @Block(
 *   id = "weds_client_menu_block",
 *   admin_label = @Translation("Weds Client Menu Block"),
 * )
 */
class WedsClientMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

 
  public function build() {
  	/*if( !Weds::is_wedding_site() ) {
  		return null;
  	}*/
    if (!$user = Weds::weds_user())
		return NULL;
	$client_menu_list = WedsClientMenu::get_client_menu_list($user->id());
    $block = [
      '#client_menu_list' => $client_menu_list,
      '#aa' => 'Aaa',
      '#theme' => 'weds_client_menu_block',
    ];
    return $block;
  }

}