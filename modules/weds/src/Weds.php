<?php
/**
 * @file
 * Contains \Drupal\weds\Weds.
 */

namespace Drupal\weds;

use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Theme\ThemeSettings;
 
class Weds {

  public static function get_wedding_site_url($uid) {
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('weds_subdomain')){
       $wedsSubdomain = \Drupal::service('weds_subdomain.manager');
       $url = $wedsSubdomain->subdomain_url($uid);
       return $url->toString();
    }
    $uri ='/user/'.$uid.'/';
    $url = Url::fromUri('internal:'.$uri);
    return $url->toString();
  }

  public static function get_admin_url($uid) {
    $uri = '/weds/'.$uid.'/edit/';
    $url = Url::fromUri('internal:'.$uri);
    return $url->toString();
  }

  public static function get_client_theme($uid){
    $user = User::load($uid);
    if(in_array('client', $user->getRoles())){
      return $user->field_theme->value;
    }
  }

  public static function is_wedding_site(){
    $route_match = \Drupal::service('current_route_match');
    $route_name = $route_match->getRouteName();

    if($route_name == 'entity.user.canonical'  || $route_name == 'weds_protected.come_to' || $route_name == 'weds_features.seating' ){
      $user = $route_match->getParameter('user');
      if(in_array('client', $user->getRoles())){
        return true;
      }
    }
    
    //  || $route_name == 'weds_protected.subdomain'
    if($route_name == 'weds.subdomain' || $route_name == 'weds_protected.subdomain'){
      $wedsSubdomain = \Drupal::service('weds_subdomain.manager');
      $domain = $route_match->getParameter('domain');
      if ($wedsSubdomain->getUserBySubdomain($domain)){
        return true;
      }
    }
   /* if( strrpos($route_name, 'weds') !== false ){
     if( !empty($user = $route_match->getParameter('user')) ) {
        if(in_array('client', $user->getRoles())){
          return true;
        }
      }
    }*/
    return false;
  }


  public static function weds_user(){ 
    $route_match = \Drupal::service('current_route_match');
    $route_name = $route_match->getRouteName();

    //admin
    if($route_name == 'weds.edit') {
      return $route_match->getParameter('user');
    }
    //site
    if( self::is_wedding_site() ) {
      if($route_name == 'entity.user.canonical') {
        return $route_match->getParameter('user');
      }
      if($route_name == 'weds.subdomain' || $route_name == 'weds_protected.subdomain') {
        $wedsSubdomain = \Drupal::service('weds_subdomain.manager');
        $domain = $route_match->getParameter('domain');
        return $wedsSubdomain->getUserBySubdomain($domain);
      }
    }
    if($route_name == 'weds_features.seating') {
      return $route_match->getParameter('user');
    }
    //register
    if($route_name == 'weds.register') {
      return \Drupal::currentUser();
    }

    return false;
  }

  public static function check_owner(){
    $user = \Drupal::currentUser();
    $route_match = \Drupal::service('current_route_match');
    $route_name = $route_match->getRouteName();
    return ( (self::is_wedding_site() || $route_name == 'weds.edit') 
              && self::weds_user()->id() == $user->id() 
              && in_array('client', $user->getRoles()) );
  }


  public static function theme_vars($theme, $uid) {
    $theme_settings = \Drupal::config($theme . '.settings')->get();
    $global_settings = Array();

    foreach ($theme_settings as $key => $value) {
      if(strpos($key, $theme) !== false) {
        $global_settings[$key] = $value;
      }
    }

    $vars = array();
    $weds_theme_settings = array();
    $weds_theme_settings = Weds::get_theme_settings($uid, $theme);
    foreach($global_settings as $key => $value){
      $var_name = str_replace($theme.'_', '', $key);
      $vars[$var_name] = (isset($weds_theme_settings[$key]) && $weds_theme_settings[$key]) ? $weds_theme_settings[$key] : $value;
      //! KISS !
      if(strpos($key, 'image')) {
        if (!empty($value) && $file = \Drupal\file\Entity\File::load($vars[$var_name][0]))
          $vars[$var_name] = $file->getFileUri();
      }
    }
    return $vars;
  }


  public static function get_theme_settings($uid, $theme){
    $query = \Drupal::database()->select('weds_theme_settings', 'wts');
    $query->addField('wts', 'settings');
    $query->condition('wts.theme', $theme);
    $query->condition('wts.uid', $uid);
    $value = $query->execute()->fetchField(); 
                        
    if(!$value) return array();

    return(unserialize($value));
  }

  public static function set_theme_settings($uid, $theme, $setings){
    $query = \Drupal::database()->select('weds_theme_settings', 'wts');
    $query->fields('wts', ['settings']);
    $query->condition('wts.uid', $uid);
    $query->condition('wts.theme', $theme);
    $count = $query->countQuery()->execute()->fetchField();
    if($count){
      $query = \Drupal::database()->update('weds_theme_settings');
      $query->fields([
        'settings' => serialize($setings),
      ]);
      $query->condition('uid', $uid);
      $query->condition('theme', $theme);
      $query->execute();
    }
    else {
      $query = \Drupal::database()->insert('weds_theme_settings');
      $query->fields([
        'uid' => $uid,
        'theme' => $theme,
        'settings' => serialize($setings),
      ]);
      $query->execute();
    }
  }
 
  // Это временное решение для храния данных!
  public static function weds_edit_data() {
  	return Array (
      'account' => array(
        'title' => 'Аккаунт',
        'vip' => FALSE,
        'form_mode' => 'weds_edit_account',
      ),
      'weds_site' => array(
        'title' => 'Настройки сайта',
        'vip' => FALSE,
        'vip_fields' => array(
          'weds_protected',
        ),
        'form_mode' => 'weds_edit_site',
      ),
      'weds_about' => array(
        'title' => 'Знакомство',
        'vip' => FALSE,
        'vip_fields' => array(
          'field_groom_social',
          'field_bride_social',
        ),
        'form_mode' => 'weds_edit_about',
      ),
      'weds_event' => array(
        'title' => 'Место и время',
        'vip' => FALSE,
        'form_mode' => 'weds_edit_event',
      ),
      'weds_story' => array(
        'title' => 'Наша история',
        'vip' => FALSE,
        'form_mode' => 'weds_edit_story',
      ),
      'weds_program' => array(
        'title' => 'Программа свадьбы',
        'vip' => FALSE,
        'form_mode' => 'weds_edit_program',
      ),
      'weds_photoalbum' => array(
        'title' => 'Фотоальбом',
        'vip' => TRUE,
        'form_mode' => 'weds_edit_photoalbum',
      ),
      'weds_rsvp' => array(
        'title' => 'RSVP',
        'vip' => TRUE,
        'form_mode' => 'weds_edit_rsvp',
      ),
      'weds_seating' => array(
        'title' => 'Рассадка гостей',
        'vip' => FALSE,
        'form_mode' => 'weds_edit_seating',
      ),
      'weds_dresscode' => array(
        'title' => 'Дресс-код',
        'vip' => TRUE,
        'form_mode' => 'weds_edit_dresscode',
      ),
    );
  }
}