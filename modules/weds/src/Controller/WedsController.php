<?
 /**
  * @file
  * Contains \Drupal\weds\Controller\WedsController.
  */

namespace Drupal\weds\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;

class WedsController extends ControllerBase {
  public function content() {
    $output = array();

    $output['#title'] = 'HelloWorld page title';

    $output['#markup'] = 'Hello World!';

    return $output;
  }

}