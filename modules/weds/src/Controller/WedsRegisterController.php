<?
 /**
  * @file
  * Contains \Drupal\weds\Controller\WedsRegisterController.
  */

namespace Drupal\weds\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormState;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\weds\Weds;

class WedsRegisterController extends ControllerBase {
  public function content() {
    // $request = new Request;
    // $query_parameters = $request->query->all();
    // ksm($query_parameters);

    $uid = \Drupal::currentUser()->id();


    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $step = $user->get('field_weds_client_step')->value;
    $roles = $user->getRoles();

    if (in_array('client', $roles)) {
      $response = new TrustedRedirectResponse(Weds::get_wedding_site_url($uid));
      $response->send();
    }

    // ksm('Controller');
    // ksm($step);
    // ksm($roles);

    //$form_state = new FormState();
    
    $step_info = $this->steps();
    $step_count = count($step_info);
    
    if($uid == 0 ) {
      $user = \Drupal::entityTypeManager()->getStorage('user')->create([]);
      $step = 0;
    }
    else {
      $user = \Drupal::entityTypeManager()
        ->getStorage('user')
        ->load($uid);
      
      $step = $user->field_weds_client_step->value;

      $step = (int)$step;
      //$step = 3;

      if( $step == 100 ||  in_array('client', $user->getRoles()) ) {
        //dsm ( Weds::get_wedding_site_url($uid) );
        //return new TrustedRedirectResponse(Weds::get_wedding_site_url($uid));
        //return new RedirectResponse(Weds::get_wedding_site_url($uid));
        return new RedirectResponse(\Drupal::url('user.page'));
      }
    } //dsm($step);
    //$step = 1;

    

   /* $form_object = \Drupal::entityTypeManager()
        ->getFormObject('user', $step_info[$step]['form_mode'])
        ->setEntity($user);*/

    //$form = \Drupal::formBuilder()->getForm($form_object);
    $form = $this->entityFormBuilder()->getForm($user, $step_info[$step]['form_mode']);

    //HTML индентификатор для шага
    $step_id = 'register_step_'.$step;

    $notification = '<div class="notification"><div class="alert alert-info"><i class="mukam-pen pull-left">&nbsp;</i>К СВЕДЕНИЮ: Любую введенную вами информацию впоследствии можно будет изменить через Меню - Настройки.</div></div><div class="paddingtop40"></div>';
    $form['#prefix'] = $notification;
    $form['#prefix'] .= '<div id="weds-reg-form-wrapper"><div id="'.$step_id.'"></div>';
  
    $step_titles = '';
    foreach($step_info as $i => $step_inf ) {
      $title = $step_inf['title'];
      $class = 'step';
      if ($i < $step) {
        $class .= ' complete';
      }
      elseif($i == $step) {
        $class .= ' active';
      }
      else {
        $class .= ' coming';
      }
      $count = ++$i;
      $title = '<li class="'.$class.'">'. '<a><span class="step-title">' . $title . '</span><span class="step-num"> ( шаг ' . $count . ' из '. $step_count .' ) </span></a></li>';
      $step_titles .= $title;
    }
    $form['#prefix'] .= '<ul class="nav nav-tabs" id="progress-info">'.$step_titles.'</ul>';
    $form['#suffix'] = '</div>';

    $form['step_title'] = array(
      '#markup' => '<h3 class="step-title">'.$step_info[$step]['title'].'</h3>',
      '#weight' => -100,
    );

    
    return $form;
  }


  protected function steps() {
  $step_info = array(
    array(
      'title' => 'Регистрация',
      'form_mode' => 'weds_register_step0',
    ),
    array(
      'title' => 'Знакомство',
      'form_mode' => 'weds_register_step1',
    ),
    array(
      'title' => 'Выбор шаблона',
      'form_mode' => 'weds_register_step2',
    ),
    array(
      'title' => 'Дата, время и место',
      'form_mode' => 'weds_register_step3',
    ),
    array(
      'title' => 'Программа свадьбы',
      'form_mode' => 'weds_register_step4',
    ), 
    array(
      'title' => 'Ура, готово!',
      'form_mode' => 'weds_register_finish',
    ),
  );

  /* Шаг оплаты. Костыль!!*/
  $user = \Drupal::currentUser();
  if( isset($_GET['premium']) ||  in_array('vip', $user->getRoles()) ) {
    $pay_step = array(
      'title' => 'Оплата',
      'form_mode' => 'weds_register_vip',
    );

    array_splice($step_info, 1, 0, array($pay_step));
  }
  /* End Костыль */

  return $step_info;
}

}  