<?php

namespace Drupal\weds\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\user\UserStorageInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * User registration password controller class.
 */
class WedsConfirmAccount extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * Constructs a UserController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Entity\Query\QueryInterface $entity_query
   *   The Entity Query.
   */
  public function __construct(DateFormatterInterface $date_formatter, UserStorageInterface $user_storage, QueryInterface $entity_query) {
    $this->dateFormatter = $date_formatter;
    $this->userStorage = $user_storage;
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('entity.manager')->getStorage('user'),
      $container->get('entity.query')->get('user', 'AND')
    );
  }

  /**
   * Confirms a user account.
   *
   * @param int $uid
   *   UID of user requesting confirmation.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The form structure or a redirect response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the login link is for a blocked user or invalid user ID.
   */
  public function confirm($uid, $timestamp, $hash) {
    $route_name = '<front>';
    $route_options = [];
    $current_user = $this->currentUser();


    // Verify that the user exists.
    if ($current_user === NULL) {
      throw new AccessDeniedHttpException();
    }
    $account = User::load($uid);


    //Нужный пользователь или аноним
    if( $current_user->id() == $account->id() || $current_user->id() === 0 ){
      
      // Redirect to weds edit page or weds register page
      if(in_array('client', $account->getRoles())) {
        $route_name = 'weds.edit';
      }
      if(in_array('client_registry', $account->getRoles())) {
        $route_name = 'user.login';
      }

      if ($account->get('field_weds_client_status')->value == 1){
        drupal_set_message(t('You are currently authenticated as user %user.', ['%user' => $current_user->getAccountName()]));
        $route_options = ['user' => $current_user->id()];
      }
      else {
        $account->field_weds_client_status->value = 1;
        $account->save();
        drupal_set_message('Ваша учетная запись активирована!');
        $route_options = ['user' => $account->id()];

        // Send Pulse ------------
        $reg_post_data = array(
          'email' => $account->mail->getValue()[0]['value'],
          'phone' => '555',
          'event_date' => date('Y-m-d'),
          'user_id' => $account->Id(),
          'activated' => 1,
        );

         $response_sendpulse = \Drupal::httpClient()->post('https://events.sendpulse.com/events/id/295f4cad748d43a5c64b76c74b0f119b', [
          'verify' => true,
          'form_params' => $reg_post_data,
            'headers' => [
              'Content-type' => 'application/x-www-form-urlencoded',
            ],
        ])->getBody()->getContents();  
        // -----------------------
      }
    }

    //Другой пользователь зашел под логином
    else {
       $reset_link_account = $this->userStorage->load($uid);
        if (!empty($reset_link_account)) {
          drupal_set_message($this->t('Another user (%other_user) is already logged into the site on this computer, but you tried to use a one-time link for user %resetting_user. Please <a href=":logout">log out</a> and try using the link again.',
            [
              '%other_user' => $current_user->getDisplayName(),
              '%resetting_user' => $reset_link_account->getDisplayName(),
              ':logout' => $this->url('user.logout'),
            ]), 'warning');
        }
        else {
          // Invalid one-time link specifies an unknown user.
          $route_name = user_registrationpassword_set_message('linkerror', TRUE);
        }
      }

      return $this->redirect($route_name, $route_options);
    }

  }

