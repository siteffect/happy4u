<?
 /**
  * @file
  * Contains \Drupal\weds\Controller\WedsEditController.
  */

namespace Drupal\weds\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormState;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\weds\Weds;

class WedsEditController extends ControllerBase {
  public function content(User $user, $item) {

    // $request = new Request;
    // $query_parameters = $request->query->all();
    // ksm($query_parameters);



    $route_match = \Drupal::service('current_route_match');
    $user = $route_match->getParameter('user');
  
    $edit_data = Weds::weds_edit_data();

    $form_object = \Drupal::entityTypeManager()
        ->getFormObject('user', $edit_data[$item]['form_mode'])
        ->setEntity($user);

    if(isset($_GET['theme'])) {
        $user->set('field_theme', $_GET['theme']);
        $user->save();
     }

    $form = \Drupal::formBuilder()->getForm($form_object);

    $notification = '<div class="alert alert-warning"><i class="mukam-sign pull-left">&nbsp;</i>Изменения, внесенные при редактировании, не сохраняются до тех пор, пока вы не нажмете кнопку "Сохранить" внизу страницы! </div>';
    $form['#prefix'] = $notification;
    $form['#prefix'] .= '<div id="weds-edit-form-wrapper">';
    $form['#suffix'] = '</div>';

    /*if ($item == 'weds_site'){
        $uid = Weds::weds_user()->id();
        $subdomain = \Drupal::service('weds_subdomain.manager');
        $domain = $subdomain->get_user_domain($uid);
    
        $form['weds_subdomain'] = array(
              '#type' => 'textfield',
              '#title' => 'Адрес сайта',
              '#collapsible' => TRUE,
              '#collapsed' => FALSE,
              '#weight' => -1,
              '#required' => TRUE,
              '#default_value' => $domain,
              '#description' => 'Самый простой и удобный вариант – это ваши имена. 
                                Просто запомнить и всем понятно. Также в качестве адреса 
                                сайта можно указать, например, дату предстоящего торжества.',
           );
          $form['#attached']['library'][] ='weds_subdomain/weds-subdomain';
          $form['#attached']['drupalSettings']['weds_subdomain']['uid'] = $uid;
    
    
          $form['actions']['submit']['#submit'][] = '_weds_subdomain_submit';}*/


  
    //ksm($form);
    return $form;
  }

  public function getTitle(User $user, $item) {
    return $user->getAccountName() . ' - Управление';
  }

  
  public function access(User $user) {
    $currentUser = \Drupal::currentUser();
    return AccessResult::allowedIf( ($currentUser->id() == $user->id()) || in_array('administrator', $currentUser->getRoles()) );
  }

}