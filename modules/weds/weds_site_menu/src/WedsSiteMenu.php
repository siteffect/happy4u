<?php
/**
 * @file
 * Contains \Drupal\weds_site_menu\WedsSiteMenu.
 */

namespace Drupal\weds_site_menu;

use Drupal\weds_blocks\WedsBlocks;
use Drupal\weds\Weds;
use Drupal\user\Entity\User;
 
class WedsSiteMenu {

  public static function get_menu_list($uid) {
    $user = User::load($uid);

    $blocks_data = WedsBlocks::weds_blocks_data();

    $user_blocks = WedsBlocks::get_user_blocks($uid);
    $empty_blocks = WedsBlocks::get_empty_blocks();

    $site_top_menu_list = Array();
    foreach($user_blocks as $block_name) {

      if ( in_array($block_name, $empty_blocks) ) {
          continue;
      }

      $title = $blocks_data[$block_name]['title'];
      $id = str_replace('weds_', '', $block_name);
      $id = str_replace('_block', '', $id);
      $site_top_menu_list[] = array(
        'title' => $title,
        'id' => $id,
        'href' => '#'.$id,
      );
    }

    return $site_top_menu_list;
  }
}