<?
namespace Drupal\weds_protected\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\weds\Weds;
use Drupal\weds_protected\WedsProtected;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('entity.user.canonical')) {
      /*$route->setDefaults(array(
         '_controller' => '\Drupal\weds_protected\Controller\WedsProtectedController::contentUserCanononical',
      ));*/ 
    }


    if ($route = $collection->get('weds.subdomain')) {
      $route->setDefaults(array(
         '_controller' => '\Drupal\weds_protected\Controller\WedsProtectedController::contentUserSubdomain',
      ));
       //$route->setPath('/page/404'); //!!костыль
    }
  }

}

