<?
 /**
  * @file
  * Contains \Drupal\weds_protected\Controller\WedsProtectedController.
  */

namespace Drupal\weds_protected\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\weds\Weds;
use Drupal\weds_protected\WedsProtected;
use Drupal\user\Entity\User;


class WedsProtectedController extends ControllerBase {
  
  public function contentUserCanononical(User $user, $key=111) {
  	if ( WedsProtected::get_enabled($user->id())) {    
  		if( WedsProtected::get_site_key($user->id()) == $key ) {
  			return user_view($user);
  		}
  		else {
        
  			$url = '/page/404';
		  	$response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
		    $response->send();
  		}
  	}
  	
  	$url = '/user/'.$user->id();
  	$response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
    $response->send();
  }


  public function contentUserSubdomain($domain, $key=111) {
    $wedsSubdomain = \Drupal::service('weds_subdomain.manager');
    $user = $wedsSubdomain->getUserBySubdomain($domain);
    if( $user = $wedsSubdomain->getUserBySubdomain($domain) ) {
      if ( WedsProtected::get_enabled($user->id()) ) {
        if ( WedsProtected::get_site_key($user->id()) == $key ) {
          return user_view($user);
        }
        else {
          $host = \Drupal::request()->getHost();
          //$url = Url::fromUri('internal:/page/404')->setAbsolute($absolute = true);
          $url = Url::fromUri('internal:/non/page')->setAbsolute($absolute = true);
          $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url->toString());
          //$response = new \Symfony\Component\HttpFoundation\RedirectResponse('system.404');
          $response->send();
        }
      }
    }
    return user_view($user);
  }
}