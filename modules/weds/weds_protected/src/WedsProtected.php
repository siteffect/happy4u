<?php
/**
 * @file
 * Contains \Drupal\weds_protected\WedsProtected.
 */

namespace Drupal\weds_protected;

use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\weds\Weds;
 
class WedsProtected {

  public static function set_settings($uid, $enabled) {
    $query = \Drupal::database()->upsert('weds_protected_settings');
    $query->fields(['uid', 'enabled']);
    $query->values([$uid, $enabled]);
    $query->key($uid);
    $query->execute();
  }


  public static function set_site_key($uid) {
    $key = substr( md5(uniqid(rand(), true)), 0, 14 );

    $query = \Drupal::database()->upsert('weds_protected_keys');
    $query->fields(['uid', 'site_key']);
    $query->values([$uid, $key]);
    $query->key($uid);
    $query->execute();

    // $path = array();
    // $path['source'] = 'user/'.$uid.'/wellcome/'.$key;
    // $path['alias'] = $key;
    // path_save($path);
  }


  public static function get_site_key($uid){
    $query = \Drupal::database()->select('weds_protected_keys', 'wpk');
    $query->addField('wpk', 'site_key');
    $query->condition('wpk.uid', $uid);
    $key = $query->execute()->fetchField();                     
    if(empty($key)) return false;
    return($key);
  }


  public static function get_enabled($uid){
    $query = \Drupal::database()->select('weds_protected_settings', 'wps');
    $query->addField('wps', 'enabled');
    $query->condition('wps.uid', $uid);
    $enabled = $query->execute()->fetchField();                     
    if(!$enabled) return false;
    return($enabled);
  }

  public static function protected_url_string($uid){
    $enabled = self::get_enabled($uid);
    $site_key = self::get_site_key($uid);
    if($enabled && $site_key) {
      $site_url = Weds::get_wedding_site_url($uid);
      $url_string = $site_url . 'p/' . $site_key;
    }
    return $url_string;
  }
  
 
  
}