<?php

/**
 * @file
 * Contains \Drupal\weds_blocks\Plugin\Block\WedsSeatingBlock.
 */


namespace Drupal\weds_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;

/**
 * @Block(
 *   id = "weds_seating_block",
 *   admin_label = @Translation("Weds - Seating"),
 * )
 */
class WedsSeatingBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

      if( Weds::is_wedding_site() ){
        $user = Weds::weds_user();
      }
      $route_match = \Drupal::service('current_route_match');
     $route_name = $route_match->getRouteName();
     if($route_name == 'weds_features.seating') {  
        $user = $route_match->getParameter('user'); 
        
     }

      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);
      $block = $vars;
      //if( empty($vars) ) return array('#markup'=>'null');
      
      if( empty($vars) ) return NULL; 
      $block['#theme'] = 'weds_seating_block';

      return $block;

  }

}
