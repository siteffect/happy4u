<?php

/**
 * @file
 * Contains \Drupal\weds_blocks\Plugin\Block\WedsAboutBlock.
 */

namespace Drupal\weds_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;

/**
 * @Block(
 *   id = "weds_about_block",
 *   admin_label = @Translation("Weds - About"),
 * )
 */
class WedsAboutBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if( Weds::is_wedding_site() ){
      $user = Weds::weds_user();
      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);
      if( empty($vars) ) return NULL;
      $block = $vars;
      $block['#theme'] = 'weds_about_block';

      //WedsBlocks::set_block_settings($user->Id(), $this->pluginId, 0);
      $status = WedsBlocks::get_block_status($user->Id(), $this->pluginId);
      return $block;
    }
    return NULL;
  }

}


/* $user = User::load(\Drupal::currentUser()->id());
  $block_data = WedsBlock::weds_block_data();
  

    $block = [
      '#groom_name' => 'Aaa',
      '#bride_name' => 'Bdd',
      '#theme' => 'weds_about_block',
    ];
    return $block;*/

// use Drupal\Core\Block\BlockBase;
// use Drupal\user\Entity\User;
// use Drupal\Core\Url;
// use Drupal\Core\Link;
// use Drupal\weds_blocks\WedsBlocks