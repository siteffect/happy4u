<?php

/**
 * @file
 * Contains \Drupal\weds_blocks\Plugin\Block\WedsPhotoalbumBlock.
 */

namespace Drupal\weds_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;

/**
 * @Block(
 *   id = "weds_photoalbum_block",
 *   admin_label = @Translation("Weds - Photoalbum"),
 * )
 */
class WedsPhotoalbumBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    if( Weds::is_wedding_site() ){
      $user = Weds::weds_user();
      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);
      $block = $vars;
      if( empty($vars) ) return NULL;
      $block['#theme'] = 'weds_photoalbum_block';
      return $block;
    }
    return NULL;
  }

}
