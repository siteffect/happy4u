<?php

/**
 * @file
 * Contains \Drupal\weds_blocks\Plugin\Block\WedsEventBlock.
 */


// Пространство имён для нашего блока.
// helloworld - это наш модуль.
namespace Drupal\weds_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;

/**
 * @Block(
 *   id = "weds_event_block",
 *   admin_label = @Translation("Weds - Event"),
 * )
 */
class WedsEventBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    if( Weds::is_wedding_site() ){
      $user = Weds::weds_user();
      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);

      //Добавить карту
      $entity_type = 'user';
      $entity_id =  $user->id();
      $field_name = 'field_address';

      $source = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
      $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $output = '';

      if ($source->hasField($field_name) && $source->access('view')) {
        $value = $source->get($field_name);
        $output = $viewBuilder->viewField($value, 'full');
      }
      
      $vars['#map'] = $output;

      $block = $vars;
      if( empty($vars) ) return NULL;
      $block['#theme'] = 'weds_event_block';
      // -----

      return $block;
    }
    return NULL;
  }

}
