<?php

/**
 * @file
 * Contains \Drupal\weds_blocks\Plugin\Block\WedsRsvpBlock.
 */


// Пространство имён для нашего блока.
// helloworld - это наш модуль.
namespace Drupal\weds_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;

/**
 * @Block(
 *   id = "weds_rsvp_block",
 *   admin_label = @Translation("Weds - RSVP"),
 * )
 */
class WedsRsvpBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
     if( Weds::is_wedding_site() ){
      $user = Weds::weds_user();
      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);
      $block = $vars;

      // Вар 1
      $webform = \Drupal\webform\Entity\Webform::load('rsvp_1');
      $output = \Drupal::entityManager()
        ->getViewBuilder('webform')
        ->view($webform);

      // Вар 2
      $webform1      = \Drupal::entityTypeManager()->getStorage('webform')->load('rsvp_1');
      $view_builder = \Drupal::service('entity_type.manager')->getViewBuilder('webform');
      $build        = $view_builder->view($webform1);

      $block['#rsvp1_form'] = $build;

      $webform2      = \Drupal::entityTypeManager()->getStorage('webform')->load('rsvp_2');
      $view_builder = \Drupal::service('entity_type.manager')->getViewBuilder('webform');
      $build        = $view_builder->view($webform2);

      $block['#rsvp2_form'] = $build;

      $block['#theme'] = 'weds_rsvp_block';

      return $block;
    }
    return NULL;
  }

}
