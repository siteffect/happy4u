<?php
/**
 * @file
 * Contains \Drupal\weds_blocks\WedsBlocks.
 */

namespace Drupal\weds_blocks;
use Drupal;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\field\Entity\FieldStorageConfig; //или
use Drupal\field\Entity\FieldConfig; //или ?
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\image\Entity\ImageStyle;
use Drupal\file\Entity\File;
use Drupal\weds\Weds;
 
class WedsBlocks {

  public static function block_vars($data_fields, $user){
    
    $vars = Array();
    $empty = 1;

    //ksm($user->field_groom_name->getValue()[1]['value']);
    // ksm($user->field_groom_name->getValue());
    foreach($data_fields as $i => $field_name){
      $varname = '#'.str_replace('field_', '', $field_name);

      /*if(empty($user->$field_name)) {
        ksm($field_name);
      }*/
     

      if( !$user->$field_name->isEmpty() ){
        $value = self::field_value($field_name, $user->$field_name);
        
        $vars[$varname] = $value;
        if($empty) $empty = 0;
      }
      else {
        $vars[$varname] = false;
      }
    }

    if($empty) return NULL;

    //Добавить к блоку переменные настроек темы (пока ко всем блокам!)
    $theme = Weds::get_client_theme($user->Id());
    $theme_vars = Weds::theme_vars($theme, $user->Id());
    $theme_settings = [];
    foreach ($theme_vars as $key => $value) {
      $theme_settings['#'.$key]  = $value;
    }
    $vars['#theme_settings'] = $theme_vars;

    //ksm($vars);

    //Закрыть платные поля
     /*if (module_exists('weds_vip')) {
        if ( !in_array('vip', $user->roles) ){
          $vip_fields = _weds_vip_fields_data();
          foreach ($vip_fields as $field) {
            $var_name = str_replace('field', '', $field);
            if(array_key_exists($var_name, $vars)) {
              $vars[$var_name] = false;
            }
          }
        }     
    }*/

    

    return $vars;
 }


  public static function field_value($field_name, $field, $i = 0){

    if($field_name == 'field_event_picture') {
      $field_definition = $field->getFieldDefinition();
      $field_info = FieldStorageConfig::loadByName($field->getFieldDefinition()->get('entity_type'), $field_name);

      // $default = $field_definition->getDefaultValue($field);
      //ksm($field->view());
    }

    

    $field_info = FieldStorageConfig::loadByName($field->getFieldDefinition()->get('entity_type'), $field_name);
    if($i==0) $value = Array();


    switch ( $field_info->getType()) {
        case 'datetime':
          if( !$field->getValue()) return NULL;
          $timestamp = $field->date->getTimestamp();
          $value[$i]['timestamp'] = $field->date->getTimestamp();
          $value[$i]['d'] = $field->date->format('d');
          $value[$i]['j'] = $field->date->format('j');
          $value[$i]['m'] = $field->date->format('m');
          $value[$i]['M'] = $field->date->format('M');
          $value[$i]['n'] = $field->date->format('n');
          $value[$i]['F'] = $field->date->format('F');
          $value[$i]['Y'] = $field->date->format('Y');
          $value[$i]['l'] = $field->date->format('l');
          $value[$i]['g'] = $field->date->format('g');
          $value[$i]['i'] = $field->date->format('i');
          $value[$i]['A'] = $field->date->format('A');
          $value[$i]['Hi'] = $field->date->format('H:i');
          break;
        case 'image':

          if( !$field->getValue()) {

            $default_image = $field->getSetting('default_image');
            if ($default_image && $default_image['uuid']) {
              $entityrepository = Drupal::service('entity.repository');
              $defaultImageFile = $entityrepository->loadEntityByUuid('file', $default_image['uuid']);
              if($defaultImageFile) {
                $uri = $defaultImageFile->getFileUri();
                $title = '';
              }
            }
            else {
              return NULL;
            }

          }
          else {
            $fid = ($field->getValue()[$i]['target_id']);
            $file = \Drupal\file\Entity\File::load($fid);
            $uri = $file->getFileUri();

            $title = $field->getValue()[$i]['title'];
          }
         
          //$field_conf = FieldConfig::loadByName('user', 'user', $field_name);
          //ksm($field_conf->getItemDefinition());

          $image_styles = ImageStyle::loadMultiple();
          $image_style = $field->view('default')[0]['#image_style'];
          $theme_image_style = \Drupal::service('theme.manager')->getActiveTheme()->getName().substr($image_style, strpos($image_style, '__'));
          if(array_key_exists($theme_image_style, $image_styles)){
            $image_style = $theme_image_style;
          }
          


          $url = \Drupal\image\Entity\ImageStyle::load($image_style)->buildUrl($uri);

          $image_path = file_url_transform_relative(file_create_url($uri));
          
          $value[$i] = array(
            'image_file_url' => file_url_transform_relative(file_create_url($uri)),
            'uri' => $uri,
            'image_style_url' => \Drupal\image\Entity\ImageStyle::load($image_style)->buildUrl($uri),
            'title' => $title,
          );
          break;

        case 'string':
          if( !$field->getValue()) return NULL;
          $value[$i] = $field->getValue()[$i]['value'];
          break;

        case 'string_long':
        case 'text_long':
          if( !$field->getValue()) return NULL;
          //$value[$i] = $field->getValue()[$i]['value'];
          $value[$i] = $field->view()[$i];
          break;

        case 'field_collection':
            if( !$field->getValue()) return NULL;
            // if ( is_numeric($field['und'][$i]['value'])) {            
            $collection = FieldCollectionItem::load($field->getValue()[$i]['value']);

            foreach($collection->getFields() as $fld_name=>$fld){
              if( strpos($fld_name, 'field' ) !== false && $fld_name != 'field_name'){
                $value[$i][str_replace('field_', '', $fld_name)] = self::field_value($fld_name, $fld);
              }
            }
            break;
        default:
          $value[$i] = NULL;
          break;
      }
      
      if( $field_info->getCardinality() == 1){
        return $value[0];
      }
      if( count( $field->getValue()) < $i+2 ){
        return $value;
      }
      return $value + self::field_value($field_name, $field, $i+1); 
  }


  public static function get_weds_regions(){
    $theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();
    $weds_regions = Array();
    $regions = array_keys(system_region_list($theme, $show = REGIONS_ALL));
    foreach ($regions as $region){
      if(strpos($region, 'weds_')!== false){
        $weds_regions[] = $region;
      }
    }
    return $weds_regions;
  }

  public static function get_block_status($uid, $block_name){
    $query = \Drupal::database()->select('weds_block_settings', 'wbs');
    $query->addField('wbs', 'desabled');
    $query->condition('wbs.uid', $uid);
    $query->condition('wbs.block_name', $block_name);
    $value = $query->execute()->fetchField();                     
    if(empty($value)) return 0;

    return($value);
  }


  public static function set_block_settings($uid, $block_name, $desabled) {
    if(is_null($desabled)) $desabled = 0;
    $query = \Drupal::database()->select('weds_block_settings', 'wbs');
    $query->fields('wbs', ['uid', 'block_name']);
    $query->condition('wbs.uid', $uid);
    $query->condition('wbs.block_name', $block_name);
    $count = $query->countQuery()->execute()->fetchField();

    if($count){
      $query = \Drupal::database()->update('weds_block_settings');
      $query->fields([
        'desabled' => $desabled,
      ]);
      $query->condition('uid', $uid);
      $query->condition('block_name', $block_name);
      $query->execute();
    }
    else {
      $query = \Drupal::database()->insert('weds_block_settings');
      $query->fields([
        'uid' => $uid,
        'block_name' => $block_name,
        'desabled' => $desabled,
      ]);
      $query->execute();
    }

    // $query = \Drupal::database()->upsert('weds_block_settings');
    // $query->fields(['desabled']);
    // $query->values([$desabled]);
    // $query->key($block_name);
    // $query->execute();
  }

  public static function get_disabled_blocks($uid){
      $query = \Drupal::database()->select('weds_block_settings', 'wbs');
      $query->addField('wbs', 'block_name');
      $query->condition('wbs.desabled', 1);
      $query->condition('wbs.uid', $uid);
      $disabled_blocks = $query->execute()->fetchCol();                     
      if(!$disabled_blocks) return array();
      return($disabled_blocks);
    }

    public static function get_user_blocks($uid){
      $blocks_data = self::weds_blocks_data();
      //$vip_blocks = _weds_vip_blocks_data();
      $disabled_blocks = self::get_disabled_blocks($uid);
      $user_blocks = Array();
      
      foreach ($blocks_data as $block_name => $item){
        /*if (module_exists('weds_vip')) {
          if ( in_array($block_name, $vip_blocks) ) {
            $user = user_load($uid);
            if ( !in_array('vip', $user->roles) ){
              continue;
            }
          }
        }*/
        if ( in_array($block_name, $disabled_blocks) ) {
          continue;
        }
        
        $user_blocks[] = $block_name;
      }
      return $user_blocks;
  }

  public static function get_empty_blocks(){
    $blocks_data = self::weds_blocks_data();
    $blocks_per_regions = \Drupal::service('block.repository')->getVisibleBlocksPerRegion();
    $weds_regions = WedsBlocks::get_weds_regions();
    $empty_blocks = array();
    foreach ($weds_regions as $weds_region){
      foreach ($blocks_per_regions[$weds_region] as $block) {
       
        //$block = \Drupal\block\Entity\Block::load($block->id());
        $block_content = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
        $render = \Drupal::service('renderer')->renderRoot($block_content);

        if ( !empty($render) ) {
          continue;
        }
        $empty_blocks[] = $block->getPluginId();
      }
      
    }
 
    return $empty_blocks;
      
  }

  // Временное хранилище данных
  public static function weds_blocks_data(){
    return Array (
        'weds_home_block' => array(
          'title' => 'В начало',
          'fields' => array(
            'field_groom_name',
            'field_bride_name',
            'field_wed_date',
          ),
          'variables' => array(),
        ),
        'weds_about_block' => array(
          'title' => 'О нас',
          'fields' => array(
            'field_groom_name',
            'field_groom_photo',
            'field_groom_text',
            'field_bride_name',
            'field_bride_photo',         
            'field_bride_text',
            'field_groom_social',
            'field_bride_social',
          ),
          'variables' => array(),
        ),
        'weds_event_block' => array(
          'title' => 'Место и время',
          'fields' => array(
            'field_wed_date',
            'field_zags',
            'field_address',
            'field_invite',
          ),
          'variables' => array(),
        ),
        'weds_program_block' => array(
          'title' => 'Программа',
          'fields' => array(
            'field_program',
          ),
          'variables' => array(),
        ),
        'weds_photoalbum_block' => array(
          'title' => 'Фото',
          'fields' => array(
            'field_photoalbum',
          ),
          'variables' => array(),
        ),
        'weds_story_block' => array(
          'title' => 'Наша история',
          'fields' => array(
            'field_story',
          ),
          'variables' => array(),
        ),
        'weds_rsvp_block' => array(
          'title' => 'RSVP',
          'fields' => array(
            'field_groom_name',
          ),
          'variables' => array(
            'rsvp1_form',
            'rsvp2_form',
          ),
        ),
        'weds_seating_block' => array(
          'title' => 'Рассадка',
          'fields' => array(
            'field_seating',
          ),
          'variables' => array(),
        ),
        'weds_dresscode_block' => array(
          'title' => 'Дресс-код',
          'fields' => array(
            'field_dresscode',
          ),
          'variables' => array(),
        ),
      );
  }
}