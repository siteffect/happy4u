# Rewrite install.php during installation to see if mod_rewrite is working
  RewriteRule ^core/install.php core/install.php?rewrite=ok [QSA,L]

  ###################################################################################
  # Weds settings

  RewriteCond %{REQUEST_URI} ^\/p\/([\S]{14})$
  RewriteCond %{HTTP_HOST} ^(.*)\.happy4\.u10915\.krypton\.vps-private\.net$ [NC]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ http://happy4.u10915.krypton.vps-private.net/weds/subdomain/%1/$1 [P,L]
  
  RewriteCond %{HTTP_HOST} ^(.*)\.happy4\.u10915\.krypton\.vps-private\.net$ [NC]
  RewriteCond %{REQUEST_URI} !^/weds/[0-9]+/feature/seating$
  RewriteCond %{REQUEST_FILENAME} !-f 
  RewriteRule ^(.*)$ http://happy4.u10915.krypton.vps-private.net/weds/subdomain/%1 [P,L]