(function ($) {
  Drupal.behaviors.subdomain = {
    attach: function(context, drupalSettings) {
      // console.log(drupalSettings)

      // Add JS validation for subdomain selection
      var timer;
      //var selector = Drupal.drupalSettings.subdomain ? Drupal.settings.subdomain.selector : undefined;
      var selector = 'edit-weds-subdomain';

      if (selector != undefined) {
        $('#weds-subdomain-suffix').after('<span id="subdomain-check" style="display:none;"></span>')
        $('#'+selector+':not(.processed)')
        .addClass('processed')
        //.after('<span id="subdomain-check" style="display:none;"></span>')
        .keyup(function(e) {
          $('#subdomain-check').hide();
          var v = $(this).val();
          clearTimeout(timer);
          timer = setTimeout(function() {
            Drupal.subdomainValidate(v, drupalSettings);
          }, 500);
        });

      }
    }
  }

  Drupal.subdomainValidate = function(check, drupalSettings) {
    if (check) {
      $('#subdomain-check')
        .text('Проверка...')
        .removeClass('malformed')
        .removeClass('duplicate')
        .addClass('checking')
        .show();
        //subdomain:check,sid:Drupal.settings.subdomain.sid}
      $.get('/weds/subdomain/validate', {subdomain:check,uid:drupalSettings.weds_subdomain.uid},function(data) {
        if (data.malformed) {
          $('#subdomain-check')
            //.text('Not a valid subdomain! A subdomain can only contain A through Z, 0 through 9, and dashes.')
            .text('Ошибка! Можно использовать только буквы латинского алфавита A-Z, числа и символы почеркивания.')
            .removeClass('checking')
            .addClass('malformed')
            .show();
        }
        else {
          if (data.available) {
            $('#subdomain-check')
              .text('Свободно!')
              .removeClass('checking')
              .removeClass('duplicate')
              .show();
          }
          else {
            $('#subdomain-check')
              .text('Занято!')
              .removeClass('checking')
              .addClass('duplicate')
              .show();
          }
        }
      }, 'json');
    }
    else {
      $('#subdomain-check').hide();
    }
  }
})(jQuery);
