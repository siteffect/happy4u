<?php
/**
 * @file
 * Contains \Drupal\weds_subdomain\WedsSubdomainManager.
 */

namespace Drupal\weds_subdomain;

use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\weds\Weds;
 
class WedsSubdomainManager {

  public function getSubdomainByUser($uid) {}

  public function getUserBySubdomain($subdomain) { //AccountInterface $account

  	$uid = $this->get_user_by_domain($subdomain);

  	if(empty($uid)) return false;

  	$user = \Drupal::entityTypeManager()
			->getStorage('user')
			->load($uid);
  	return $user;
  }


  public function get_current_subdomain() {
	  //$subdomain = &drupal_static(__FUNCTION__ . '__subdomain', NULL);

	  //if (!isset($subdomain)) {
	    $subdomain_length = strpos($_SERVER['HTTP_HOST'], '.' . $this->current_domain());
	    $subdomain = substr($_SERVER['HTTP_HOST'], 0, $subdomain_length);

	    $httpHost = $_SERVER['HTTP_HOST'];
	    
	    // Strip 'www' if set to use it.
	    // if (variable_get('subdomain_prepend_www', TRUE) && $subdomain == 'www') {
	    //   $subdomain = '';
	    // }
	  //}
	  return $subdomain;
   }

	public function current_domain() {
		$sessionConfig =\Drupal::service('session_configuration');
		// Get the current request.
		$request = \Drupal::request();
		// Get options from the request.
		$options = $sessionConfig->getOptions($request);
		$cookie_domain = $options['cookie_domain'];
		return trim($cookie_domain, '.');

		$httpHost = $this->negotiateActiveHostname();
    	$this->setRequestDomain($httpHost);
	}


	/**
	* Prepares subdomain for saving
	*/
	public function clean($raw, $make_unique = FALSE) {
		$clean = $raw;

		// Try to use the transliteration module if available.
		/*if (function_exists('transliteration_get')) {
		  $clean = transliteration_get($clean, '-', language_default('language'));
		}*/

		// Replace spaces with dashes, and make sure we don't have multiple dashes in a row.
		$clean = str_replace(' ', '-', $clean);
		$clean = preg_replace('/--+/i', '-', $clean);

		/*
		Hostname spec:
		The original specification of hostnames in RFC 952, mandated that labels could not start with a digit or with a hyphen, and must not
		end with a hyphen. However, a subsequent specification (RFC 1123) permitted hostname labels to start with digits.
		*/

		// Remove any remaining invalid characters
		$clean = preg_replace("/[^a-z0-9-]/i", "", $clean);

		// Remove any leading dash
		$clean = preg_replace("/^-/i", "", $clean);

		// Remove any trailing dash
		$clean = preg_replace("/-$/i", "", $clean);

		// Are we going to allow the subdomain to start with a numeral (RCC 1123)?
		/*if (!variable_get('subdomain_allow_rfc1123', FALSE)) {
		  // Clean until nothing is replaced.
		  $dirty = TRUE;
		  while ($dirty) {
		    // Remove leading digits
		    $clean = preg_replace("/^[0-9]+/i", "", $clean, -1, $dirty);

		    // Remove any exposed leading dash
		    $clean = preg_replace("/^-/i", "", $clean);
		  }
		}*/

		// Make sure we return a lowercase subdomain
		$clean = strtolower($clean);

		if (!empty($clean) && $make_unique) {
		  // If the subdomain already exists, generate a new, hopefully unique, variant
		  if ($this->get_user_by_domain($clean)) {
		    $maxlength = 250;
		    $original_clean = $clean;

		    $i = 0;
		    do {
		      // Append an incrementing numeric suffix until we find a unique alias.
		      $unique_suffix = '-' . $i;
		      $clean = truncate_utf8($original_clean, $maxlength - drupal_strlen($unique_suffix, TRUE)) . $unique_suffix;
		      $i++;
		    } while ($this->get_user_by_domain($clean));
		  }
		}

		return $clean;
	}

	public function subdomain_url($uid) {
		if($domain = $this->get_user_domain($uid)) {
	        $session_configuration = \Drupal::service('session_configuration');
	        $session_configuration_options = $session_configuration->getOptions(\Drupal::request());
	        $cookie_domain = $session_configuration_options['cookie_domain'];

	        $uri ='/';
		    $url = Url::fromUri('internal:'.$uri);
		    $url->setAbsolute();
		    $url->setOption('base_url', 'http://'.$domain.$cookie_domain);
    	}
    	else {
    		$uri ='/user/'.$uid.'/';
    		$url = Url::fromUri('internal:'.$uri);
    	}
    	
    	return $url;
	}


	public function set_user_domain($uid, $domain) {
		$query = \Drupal::database()->upsert('weds_subdomain');
		$query->fields(['uid', 'domain']);
		$query->values([$uid, $domain]);
		$query->key($uid);
		$query->execute();
	}

	public function get_user_domain($uid){
		$query = \Drupal::database()->select('weds_subdomain', 'wsd');
		$query->addField('wsd', 'domain');
		$query->condition('wsd.uid', $uid);
		$domain = $query->execute()->fetchField();                     
		if(empty($domain)) return false;
		return($domain);
	}

	public static function get_user_by_domain($domain){
		$query = \Drupal::database()->select('weds_subdomain', 'wsd');
		$query->addField('wsd', 'uid');
		$query->condition('wsd.domain', $domain);
		$domain = $query->execute()->fetchField();                     
		if(empty($domain)) return false;
		return($domain);
	}

}