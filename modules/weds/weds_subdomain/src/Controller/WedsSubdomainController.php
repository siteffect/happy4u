<?
 /**
  * @file
  * Contains \Drupal\weds_subdomain\Controller\WedsSubdomainController.
  */

namespace Drupal\weds_subdomain\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\weds\Weds;
use Drupal\weds_protected\WedsProtected;
use Drupal\user\Entity\User;
use Drupal\weds_subdomain\WedsSubdomainManager;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;



class WedsSubdomainController extends ControllerBase {
	protected $wedsSubdomain;

	/**
	* Constructs a WedsSubdomainController object.
	*
	* @param  \Drupal\weds_subdomain\WedsSubdomainManager
	*   An alias manager for looking up the system path.
	*/
	public function __construct(WedsSubdomainManager $weds_subdomain) {
	   $this->wedsSubdomain = $weds_subdomain;
  	}

  	
  	/**
   * {@inheritdoc}
   */
	public static function create(ContainerInterface $container) {
		return new static(
		  $container->get('weds_subdomain.manager')
		); 
	}

	public function contentSubdomain($domain) {


			// $subd = \Drupal::service('path.alias_manager')->getAliasByPath($front_uri);
			// return user_view(48);
		if($user = $this->wedsSubdomain->getUserBySubdomain($domain)) {
			return user_view($user);
		}

		else {
			//throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
			$url = Url::fromRoute('system.404');
			$response = new \Symfony\Component\HttpFoundation\RedirectResponse($url->toString());
			$response->send();
		}


		$output = array();

	    $output['#title'] = 'HelloWorld page title';

	    $output['#markup'] = 'Hello World!';

	    return $output;
	}


	/**
	* Callback for `my-api/get.json` API method.
	*/
	public function js_validate( Request $request ) {

		$response['available'] = TRUE;
		$response['malformed'] = FALSE;
		$response['method'] = 'GET';

		//return new JsonResponse( $response );

		$subdomain = strtolower(trim($_GET['subdomain']));

		$is_malformed = ($this->wedsSubdomain->clean($subdomain) != $subdomain);
		//$is_reserved = in_array($subdomain, subdomain_get_reserved_subdomains());
		$is_reserved = FALSE;
		$uid = $this->wedsSubdomain->get_user_by_domain($subdomain);

		// Subdomain is valid if it doesn't exist or it's the one being edited
		$response['available'] = !$is_reserved && (!$uid || $uid == $_GET['uid']);
		$response['malformed'] = $is_malformed;
		$response['ok'] = 'ok';
		$response['method'] = 'GET';

		return new JsonResponse( $response );
	}



}