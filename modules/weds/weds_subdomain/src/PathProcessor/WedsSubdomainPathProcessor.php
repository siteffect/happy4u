<?

namespace Drupal\weds_subdomain\PathProcessor;
 
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\node\Entity\User;
use Drupal\Core\Config\Config;
use Drupal\weds\Weds;
use Drupal\weds_subdomain\WedsSubdomainManager;

class WedsSubdomainPathProcessor implements InboundPathProcessorInterface,  OutboundPathProcessorInterface {
   
   protected $wedsSubdomain;

   protected $frontpage_alias;

  /**
   * Constructs a WedsSubdomainPathProcessor object.
   *
   * @param  \Drupal\weds_subdomain\WedsSubdomainManager
   *   An alias manager for looking up the system path.
   */
  public function __construct(WedsSubdomainManager $weds_subdomain) {
    $this->wedsSubdomain = $weds_subdomain;

    $config = \Drupal::config('system.site');
	$front_uri = $config->get('page.front');
	$this->frontpage_alias = \Drupal::service('path.alias_manager')->getAliasByPath($front_uri);
  }


// ...


   /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
  	
    // Do something.
    //$path = '/user/30';
    //$path = $this->wedsSubdomain->getSubdomainByUser(48, $path);


    $route_match = \Drupal::service('current_route_match');
	$route_name = $route_match->getRouteName();
		//ksm($route_name);

	if (preg_match('/.*\\/weds\\/subdomain\\/([a-zA-Z0-9_]+)$/', $path, $matches)) {

		if($user = $this->wedsSubdomain->getUserBySubdomain($matches[1])) {
			//$request->query->set('user', $user->id());
			//$path = '/user';
      //$path = '/user/'.$user->id();
      //$path='/';
		}
	}

 	if($path == $this->frontpage_alias) {
 		/*if ($curr_subdomain == 'zheka')
    		$path = '/user/48';
    	else $path = $this->frontpage_alias;*/
    }

    /*$host = $request->getHost();
	if(strpos($host, 'zheka') !== false) {
		$path = '/user/48';
	}*/

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {

    if (preg_match('/.*\\/user\\/([0-9]+)$/', $path, $matches)) {
      


      if($domain = $this->wedsSubdomain->get_user_domain($matches[1])) {
        $session_configuration = \Drupal::service('session_configuration');
        $session_configuration_options = $session_configuration->getOptions(\Drupal::request());
        $cookie_domain = $session_configuration_options['cookie_domain'];

        // $path = '/';
        // $options['absolute'] = TRUE;
        // $options['base_url'] = 'http://'.$domain.$cookie_domain;
        //$host = \Drupal::request()->getHost();
      }
    }

    if (strpos($path, 'subdomain')) {
      //      $session_configuration = \Drupal::service('session_configuration');
      // $session_configuration_options = $session_configuration->getOptions(\Drupal::request());
      // $cookie_domain = $session_configuration_options['cookie_domain'];
      // $options['absolute'] = TRUE;
      // $options['base_url'] = 'http://'.$cookie_domain;
    }
    $this->wedsSubdomain->get_current_subdomain();
    $route_match = \Drupal::service('current_route_match');
    $route_name = $route_match->getRouteName();

    /*if( $route_name == 'weds.subdomain') {
      $session_configuration = \Drupal::service('session_configuration');
      $session_configuration_options = $session_configuration->getOptions(\Drupal::request());
      $cookie_domain = $session_configuration_options['cookie_domain'];
      $options['absolute'] = TRUE;
      $options['base_url'] = 'http://'. substr($cookie_domain, 1);
    }*/
    
    if (strpos($path, 'user') !== FALSE ) {
      $options['absolute'] = TRUE;
    }

    return $path;
  }

}