<?
namespace Drupal\weds_subdomain\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Always deny access to '/user/logout'.
    // Note that the second parameter of setRequirement() is a string.

    if ($route = $collection->get('weds_protected.come_to')) {
       //$route->setPath('subdomain/jeka');
       /*$route->setDefaults(array(
        '_controller' => '\Drupal\weds\Controller\WedsSubdomainController::content',
        ));*/
    }

    if ($route = $collection->get('entity.user.canonical')) {
      //ksm( $route_match->getParameter('user')->id());

      /*$route_match = \Drupal::service('current_route_match');
    $route_name = $route_match->getRouteName();
    return ( (self::is_wedding_site() || $route_name == 'weds.edit') 
              &&  $route_match->getParameter('user')->id() == $user->id() 
              && in_array('client', $user->getRoles()) );*/
              
       //$route->setPath('/a');
    }
     if ($route = $collection->get('user.login')) {
      //$route->setPath('/auth');
    }
    if ($route = $collection->get('view.frontpage.page_1')) {
      //ksm($route->getHost());
      //$route->setPath('/user/{uid}');
      //$collection->add('entity.user.canonical', $route);
      /*$route->setDefaults(array(
        '_controller' => '\Drupal\weds\Controller\WedsConfirmAccount::confirm',
      ));*/
    }
  }

}

