<?php

namespace Drupal\weds_subdomain\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\user\Entity\User;

/**
 * Sets the Stark for frontpage.
 */
class WedsSubdomainThemes implements ThemeNegotiatorInterface {

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  protected $wedsSubdomain;

  /**
   * StarkForFront constructor.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   */
  public function __construct(PathMatcherInterface $pathMatcher) {
    $this->pathMatcher = $pathMatcher;
    $this->wedsSubdomain =  \Drupal::service('weds_subdomain.manager');
  }


  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $routeMatch) {
    if ($routeMatch->getRouteName() == 'weds.subdomain') {
      $domain = $routeMatch->getParameter('domain');

      if($user = $this->wedsSubdomain->getUserBySubdomain($domain)) {
        if(!empty($user) && in_array('client', $user->getRoles())){
          if($user->field_theme->value)
            return true;
        }
      }
    }   
  }


  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $routeMatch) {
    $domain = $routeMatch->getParameter('domain');
    $user = $this->wedsSubdomain->getUserBySubdomain($domain);
    $theme = $user->field_theme->value;

    return $theme;
  }
}