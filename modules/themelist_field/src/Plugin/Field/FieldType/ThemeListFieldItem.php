<?php

/**
 * @file
 * Contains Drupal\themelist_field\Plugin\Field\FieldType\ThemeListFieldItem.
 */

namespace Drupal\themelist_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * @FieldType(
 *   id = "themelist_field",
 *   label = @Translation("Theme list field"),
 *   module = "themelist_field",
 *   description = @Translation("Theme list."),
 *   category = @Translation("Theme"),
 *   default_widget = "themelist_field_list_widget",
 *   default_formatter = "themelist_field_default_formatter"
 * )
 */
class ThemeListFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   *
   * @see https://www.drupal.org/node/159605
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   *
   * Это указывает Drupal на то, как нужно хранить значения этого поля.
   * Например integer, string или any.
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Тема'));

    return $properties;
  }
}