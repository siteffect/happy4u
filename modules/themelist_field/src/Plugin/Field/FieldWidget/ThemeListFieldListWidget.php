<?php

/**
 * @file
 * Contains \Drupal\my_color_field\Plugin\Field\FieldWidget\ThemeListFieldListWidget.
 */

namespace Drupal\themelist_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "themelist_field_list_widget",
 *   module = "themelist_field",
 *   label = @Translation("Themes Select list"),
 *   field_types = {
 *     "themelist_field"
 *   }
 * )
 */
class ThemeListFieldListWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   *
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $theme_handler = \Drupal::service('theme_handler');
    $list_themes_all = $theme_handler->listInfo(); 
    $list_themes = Array();
    $user = \Drupal::currentUser();;
    foreach($list_themes_all as $t_name => $theme){
      if($theme->status) {
        if(isset($theme->info['package']) && $theme->info['package'] == 'Weds'){
          if(isset($theme->info['show']) && $theme->info['show'] == 0){
            if ( !in_array('prewuser', $user->roles) ) continue;
          }
          $list_themes[$t_name] = $theme;
        }
      }
    }

    $first_theme = array_shift($list_themes);
    $options = Array($first_theme->getName() => $first_theme->getName());
    foreach ($list_themes as $t_name => $t_obj) {
        $options[$t_name] = $t_obj->info['name'];
    }
    $element += [
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : '',
      '#options' => $options,
    ];

    return ['value' => $element];
  }
}