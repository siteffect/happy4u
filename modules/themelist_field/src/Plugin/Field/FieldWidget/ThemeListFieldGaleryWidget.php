<?php

/**
 * @file
 * Contains \Drupal\themelist_field\Plugin\Field\FieldWidget\ThemeListFieldGaleryWidget.
 */

namespace Drupal\themelist_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "themelist_field_galery_widget",
 *   module = "themelist_field",
 *   label = @Translation("Themes Galery"),
 *   field_types = {
 *     "themelist_field"
 *   }
 * )
 */
class ThemeListFieldGaleryWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   *
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    //$element = parent::formElement($items, $delta, $element, $form, $form_state);
    $theme_handler = \Drupal::service('theme_handler');
    $list_themes_all = $theme_handler->listInfo(); 
    $list_themes = Array();
    $user = \Drupal::currentUser();
    foreach($list_themes_all as $t_name => $theme){
      if($theme->status) {
        if(isset($theme->info['package']) && $theme->info['package'] == 'Weds'){
          if(isset($theme->info['show']) && $theme->info['show'] == 0){
            if ( !in_array('prewuser', $user->roles) ) continue;
          }
          $list_themes[$t_name] = $theme;
        }
      }
    }

    // $first_theme = array_shift($list_themes);
    // ksm($first_theme);
    // $options = Array($first_theme->getName() => $first_theme->getName());
    $options = Array();
    foreach ($list_themes as $t_name => $t_obj) {
        $options[$t_name] = $t_obj->info['name'];
    }

    $element += [
      '#type' => 'radios',
      '#title' => 'Тема',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : '',
      '#options' => $options,
      //'#process' => [[get_class($this), 'process']],
      '#theme' => 'themelist_field_galery',
    ];

    return ['value' => $element];
  }


  public static function process($element, FormStateInterface $form_state, $form) {
    //ksm($element);
    //$element['#theme'] = 'themelist_field_galery';  
    return $element;
  }

}

