<?php

/**
 * @file
 * Contains \Drupal\themelist_field\Plugin\Field\FieldFormatter\ThemeListFieldDefaultFormatter.
 */

namespace Drupal\themelist_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/** *
 * @FieldFormatter(
 *   id = "themelist_field_default_formatter",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "themelist_field"
 *   }
 * )
 */
class ThemeListFieldDefaultFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->value,
      ];
    }

    return $element;
  }

} 