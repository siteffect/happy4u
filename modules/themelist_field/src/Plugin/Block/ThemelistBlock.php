<?php

/**
 * @file
 * Contains \Drupal\themelist_field\Plugin\Block\ThemelistBlock.
 */

namespace Drupal\themelist_field\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\weds\Weds;
use Drupal\weds_blocks\WedsBlocks;
use Drupal\user\Entity\User;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * @Block(
 *   id = "themelist_field_block",
 *   admin_label = @Translation("Themelist block"),
 * )
 */
class ThemelistBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /*if( Weds::is_wedding_site() ){
      $user = Weds::weds_user();
      $block_data = WedsBlocks::weds_blocks_data()[$this->pluginId];
      $vars = WedsBlocks::block_vars($block_data['fields'], $user);
      if( empty($vars) ) return NULL;
      $block = $vars;
      $block['#theme'] = 'weds_about_block';

      //WedsBlocks::set_block_settings($user->Id(), $this->pluginId, 0);
      $status = WedsBlocks::get_block_status($user->Id(), $this->pluginId);
      return $block;
    }*/

    $block = [
      '#type' => 'markup',
      '#markup' => 'My <strong>example</strong> content.'
    ];

    $user = \Drupal::currentUser();
    if ( !in_array('client', $user->getRoles()) ) {
      $choose_theme_url =  Url::fromUri('internal:/weds/register')->setAbsolute($absolute = true); 
    }
    else {
      $choose_theme_url =  Url::fromUri('internal:/weds/'.$user->id().'/edit/weds_site')->setAbsolute($absolute = true); 
    }

    $block = [
      '#items' => $this->themelist_block_vars(),
      '#choose_theme_url' => $choose_theme_url,
      '#theme' => 'themelist_field_block_galery',
    ];

    $block['#attached']['library'][] = 'themelist_field/themelist_field';
    $block['#attached']['library'][] = 'core/drupal.dialog.ajax'; 
    return $block;
  }


  public function themelist_block_vars(){
    $theme_handler = \Drupal::service('theme_handler');
    $list_themes_all = $theme_handler->listInfo(); 
    $items = Array();
    $user = \Drupal::currentUser();

    $config = \Drupal::config('system.theme');

    foreach($list_themes_all as $t_name => $theme) {

      if($theme->status) {

         if(isset($theme->info['package']) && $theme->info['package'] == 'Weds'){
          if(isset($theme->info['show']) && $theme->info['show'] == 0){
            if ( !in_array('prewuser', $user->getRoles()) ) continue;
          }
          $item = Array();
          //ksm($theme_vars);

          $theme_settings = \Drupal::config($t_name . '.settings')->get();

          $item['theme_name'] = $t_name;
          $item['thumbnail_url'] = file_create_url($theme->info['screenshot']);

          $item['sample_path'] = $theme_settings['sample_path'];

           if ( isset($theme_settings['image']) 
              && ($fid = $theme_settings['image']) 
              && ($file = file_load($fid) )
            ) {
            $uri = $file->uri;
            $item['fullimage_url'] = file_create_url($uri);
          }
          else {
            $item['fullimage_url'] = $item['thumbnail_url'];
          }

          $item['premium'] = false; 

          if( isset($theme->info['vip']) && $theme->info['vip'] == 1 ) {
              if ( !in_array('vip', $user->getRoles()) ) {
                $item['premium'] = true;
                $menu_item['vip'] = TRUE;

                // gold_link
                $url = Url::fromUri('internal:/weds/get-vip');
                $golg_link_options = array(
                  'attributes' => array(
                    'class' => array(
                      'use-ajax',
                      'weds-premium-theme',
                    ),
                    'data-dialog-type' => array(
                      'modal',
                    ),
                    'title' => 'UpGrade!',
                  ),
                );
                $url->setOptions($golg_link_options);
                $golg_link = Link::fromTextAndUrl('', $url);
                $item['golg_link'] = $golg_link->toRenderable();


                // premium_button
              

                /*$item['premium_button'] = '<a href="/weds/get-vip" class="use-ajax" data-dialog-type="modal" title="UpGrade!"><button class="buton b_inherit buton-2 buton-mini">Premium</button></a>';*/

                // $item['premium_button'] = ctools_modal_text_button('<button class="buton b_inherit buton-2 buton-mini">Premium</button>', 'weds/get-vip/nojs', 'UpGrade!', 'ctools-modal-weds-vip-popup-style');

              }
          }

          $items[$t_name] = $item;
        }

      }

    }
    return $items;




    /*$list_themes_all = list_themes();
    $items = Array();

    global $user;
    foreach($list_themes_all as $t_name => $theme){
      if($theme->status) {
        if((isset($theme->info['package']) && $theme->info['package'] == 'Weds')){
          if(isset($theme->info['show']) && $theme->info['show'] == 0){
            if ( !in_array('prewuser', $user->roles) ) continue;
          }
          $item = Array();
          $theme_settings = variable_get('theme_'.$t_name.'_settings', array()); 
          $item['name'] = $t_name;

          $item['thumbnail_url'] = url($theme->info['screenshot']);
      
          if ( ($fid = theme_get_setting('image', $t_name))
              && ($file = file_load($fid))
          ) {
            $uri = $file->uri;
            $item['fullimage_url'] = file_create_url($uri);
          }
          else {
            $item['fullimage_url'] = $item['thumbnail_url'];
          }

          $item['sample_path'] = theme_get_setting('sample_path', $t_name);

          $item['premium'] = false;
          global $user;

          if(isset($theme->info['vip']) && $theme->info['vip'] == 1){
              if ( !in_array('vip', $user->roles) ) {
                $item['premium'] = true;
                $item['golg_link'] = ctools_modal_text_button('', 'weds/get-vip/nojs', 'UpGrade!', 'ctools-modal-weds-vip-popup-style weds-premium-theme');
                $item['premium_button'] = ctools_modal_text_button('<button class="buton b_inherit buton-2 buton-mini">Premium</button>', 'weds/get-vip/nojs', 'UpGrade!', 'ctools-modal-weds-vip-popup-style');
              }
          }
        
          $items[$t_name] = $item;
        }

      }
    }
    return $items; */
  }

}


/* $user = User::load(\Drupal::currentUser()->id());
  $block_data = WedsBlock::weds_block_data();
  

    $block = [
      '#groom_name' => 'Aaa',
      '#bride_name' => 'Bdd',
      '#theme' => 'weds_about_block',
    ];
    return $block;*/

// use Drupal\Core\Block\BlockBase;
// use Drupal\user\Entity\User;
// use Drupal\Core\Url;
// use Drupal\Core\Link;
// use Drupal\weds_blocks\WedsBlocks