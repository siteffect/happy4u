<?php

/**
 * @file
 * Contains \Drupal\multipart_register\Form\AdditionalDetailsForm.
 */

namespace Drupal\multipart_register\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\user\Entity\User;

class AdditionalDetailsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'weds-register-step1';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    self::setEntity(User::load($this->currentUser()->id()));

    /* @var $entity \Drupal\user\Entity\User */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $form_state->setRedirect(
      'entity.user.edit_form',
      ['user' => $this->entity->id()]
    );
  }
}