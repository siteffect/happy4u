   /* ---------------------------------------------------------------------- */
    /* ------------------------------- scrollToTop  ------------------------- */
    /* ---------------------------------------------------------------------- */
    (function ($) {
        $(window).on('scroll', function(){
            if ($(window).scrollTop() > 600) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });
    
        $('.scrollToTop').on('click', function(e) {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    })(jQuery);