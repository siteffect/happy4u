<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for mukam themes when admin theme is not.
 *
 * 
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */

function marriage_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  // Alter theme settings form.
  $form['weds_client'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки темы',
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['weds_admin'] = array(
    '#type' => 'conteiner',
    '#title' => 'Настройки темы (для админа)',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['weds_client']['marriage_background_image_1'] = array(
    '#title' => "Главная фоновая картинка",
    '#type' => 'managed_file',
    '#required' => FALSE,
    '#description' => "Рекомендуемый размер изображения 900 x 400",
    '#default_value' => theme_get_setting('marriage_background_image_1'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // 'file_validate_image_resolution' => array('900x400', '900x400'),
    ),
  );
   $form['weds_admin']['marriage_invite_image'] = array(
    '#title' => "Картинка в блоке \"Приглашение\"",
    '#type' => 'managed_file',
    '#required' => FALSE,
    '#description' => "Рекомендуемый размер изображения 810 x 234",
    '#default_value' => theme_get_setting('marriage_invite_image'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // 'file_validate_image_resolution' => array('810x234', '810x234'),
    ),
  );

  $form['weds_admin']['image'] = array(
    '#title' => "Image",
    '#type' => 'managed_file',
    '#required' => FALSE,
    //'#description' => "1920 x 720",
    '#default_value' => theme_get_setting('image'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['weds_admin']['sample_path'] = array(
    '#title' => "Sample path",
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => theme_get_setting('sample_path'),
  );

  $form['#submit'][] = '_marriage_form_system_theme_settings_submit';
}


function _marriage_form_system_theme_settings_submit ($form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $image_settings = [
    'marriage_background_image_1',
    'marriage_invite_image',
    'image',
  ];

  foreach ($image_settings as $image_setting) {
    $value = $form_state->getValue($image_setting, []);
    if ($value) {
      $fid = $value[0];
      $file = \Drupal\file\Entity\File::load($fid);
      $file->setPermanent();
      $file->save();
    }
  }
}
