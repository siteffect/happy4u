//    /* ---------------------------------------------------------------------- */
//     /* ------------------------------- scrollToTop  ------------------------- */
//     /* ---------------------------------------------------------------------- */
//     (function ($) {
//         $(window).on('scroll', function(){
//             if ($(window).scrollTop() > 600) {
//                 $('.scrollToTop').fadeIn();
//             } else {
//                 $('.scrollToTop').fadeOut();
//             }
//         });
    
//         $('.scrollToTop').on('click', function(e) {
//             $('html, body').animate({
//                 scrollTop: 0
//             }, 800);
//             return false;
//         });
//     })(jQuery);

// (function ($) {
//  /* Scroll to Main Menu
//         ==================================================================================== */
//         $('#navigation a[href*=#],#navigation-dotted a[href*=#]').click(function(event) {
//             var $this = $(this);
//             if ($this.parents("#navigation-dotted") && $this.parents("#navigation-dotted").length > 0) { // check to see if navigation is dotted,if yes no offset
//                 var offset = 0;
//             } else {
//                 var offset = -56;
//             }

//             $.scrollTo($this.attr('href'), 650, {
//                 easing: 'swing',
//                 offset: offset,
//                 'axis': 'y'
//             });
//             event.preventDefault();

//             // For mobiles and tablets, do the thing!
//             if ($(window).width() < 1025) {
//                 $('#menu-toggle-wrapper').toggleClass('open');
//                 $('#navigation').slideToggle(200);
//             }
//         });
// })(jQuery);


// /* Fix for older browsers */
// if (!Array.prototype.indexOf) {
//     Array.prototype.indexOf = function(obj, start) {
//         for (var i = (start || 0), j = this.length; i < j; i++) {
//             if (this[i] === obj) {
//                 return i;
//             }
//         }
//         return -1;
//     }
// }

(function($) {

    "use strict";

    $(document).ready(function() {

        /* Preloader - remove this and loaderOverlay div in html file, to disable loader.
        ==================================================================================== */
        if (typeof imagesLoaded != 'undefined') {
            imagesLoaded($('body'), function() {
            	wow.init();
                $('.loaderOverlay').fadeOut('slow');
                $('#gallery-wrapper').isotope('reLayout');
            });
        }

        /* Hero height for full and half screen
        ==================================================================================== */
        var windowHeight = $(window).height();
        $('.hero').height(windowHeight - 80);
        $('.hero.mvisible').height(windowHeight - 136);

        $(window).resize(function() {
            var windowHeight = $(window).height();
            $('.hero').height(windowHeight - 80);
            $('.hero.mvisible').height(windowHeight - 136);
        });

        $(window).on('scroll', function(){
            if ($(window).scrollTop() > 600) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });
    
        $('.scrollToTop').on('click', function(e) {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        /* Responsive Menu - Expand / Collapse on Mobile
        ==================================================================================== */
        function recalculate_height() {
            var nav_height = $(window).outerHeight();
            $("#navigation").height(nav_height - 56);
        }

        $('#menu-toggle-wrapper').on('click', function(event) {
            recalculate_height();
            $(this).toggleClass('open');
            $("body").toggleClass('open');
            $('#navigation').slideToggle(200);
            event.preventDefault();
        });

        $(window).resize(function() {
            recalculate_height();
        });





         /* Scroll to Main Menu
        ==================================================================================== */
        $('#navigation a[href*="#"],#navigation-dotted a[href*="#"]').click(function(event) {
            var $this = $(this);
            if ($this.parents("#navigation-dotted") && $this.parents("#navigation-dotted").length > 0) { // check to see if navigation is dotted,if yes no offset
                var offset = 0;
            } else {
                var offset = -56;
            }

            $.scrollTo($this.attr('href'), 650, {
                easing: 'swing',
                offset: offset,
                'axis': 'y'
            });
            event.preventDefault();

            // For mobiles and tablets, do the thing!
            if ($(window).width() < 1025) {
                $('#menu-toggle-wrapper').toggleClass('open');
                $('#navigation').slideToggle(200);
            }
        });

        /* Main Menu - Add active class for each nav depending on scroll
        ==================================================================================== */
        // $('section').each(function() {
        //     $(this).waypoint(function(direction) {
        //         if (direction === 'down') {
        //             var containerID = $(this).attr('id');
        //             /* update navigation */
        //             $('#navigation ul li a,#navigation-dotted ul li a').removeClass('active');
        //             $('#navigation ul li a[href*=#' + containerID + '],#navigation-dotted ul li a[href*=#' + containerID + ']').addClass('active');
        //         }
        //     }, {
        //         offset: '80px'
        //     });

        //     $(this).waypoint(function(direction) {
        //         if (direction === 'up') {
        //             var containerID = $(this).attr('id');
        //             /* update navigation */
        //             $('#navigation ul li a,#navigation-dotted ul li a').removeClass('active');
        //             $('#navigation ul li a[href*=#' + containerID + '],#navigation-dotted ul li a[href*=#' + containerID + ']').addClass('active');
        //         }
        //     }, {
        //         offset: function() {
        //             return -$(this).height() - 80;
        //         }
        //     });
        // });

       

        /* Scroll to Element on Page - 
        /* USAGE: Add class "scrollTo" and in href add element where you want to scroll page to.
        ==================================================================================== */
        $('a.scrollTo').click(function(event) {
            var $target = $(this).attr("href");
            event.preventDefault();
            $.scrollTo($($target), 1250, {
                offset: -56,
                'axis': 'y'
            });
        });

        /* Main Menu - Fixed on Scroll
        ==================================================================================== */
        $("#home-content").waypoint(function(direction) {
            if (direction === 'down') {
                $("#main-menu").removeClass("gore");
                $("#main-menu").addClass("dole");
            } else if (direction === 'up') {
                $("#main-menu").removeClass("dole");
                $("#main-menu").addClass("gore");
            }
        }, {
            offset: '1px'
        });

       

       
        /* Notification Messages
        ==================================================================================== */
        $(document).on('click', '.successmsg,.errormsg,.notice,.general', function() {
            var $this = $(this);
            $this.fadeOut();
        });

        

        /* Isotope Portfolio
        ==================================================================================== */
        var $container = $('#gallery-wrapper');
        $container.isotope({
            itemSelector: '.block',
            layoutMode: 'sloppyMasonry',
            // filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('#gallery-filter a').click(function() {
            $('#gallery-filter li.active').removeClass('active');
            $(this).parent().addClass('active');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });

       
        /* Theme Tabs
        ==================================================================================== */
        $('.tabs').each(function() {
            var $tabLis = $(this).find('li');
            var $tabContent = $(this).next('.tab-content-wrap').find('.tab-content');
            $tabContent.hide();
            $tabLis.first().addClass('active').show();
            $tabContent.first().show();
        });

        $('.tabs').on('click', 'li', function(e) {
            var $this = $(this);
            var parentUL = $this.parent();
            var scrollparentURL = $this.parent();

            var tabContent = scrollparentURL.next('.tab-content-wrap');
            parentUL.children().removeClass('active');
            $this.addClass('active');

            tabContent.find('.tab-content').hide();
            var showById = $($this.find('a').attr('href'));
            tabContent.find(showById).fadeIn();
            e.preventDefault();
        });

        /* Theme Accordion
        ==================================================================================== */
        $('.accordion').on('click', '.title', function(event) {
            event.preventDefault();
            var $this = $(this);

            if ($this.closest('.accordion').hasClass('toggle')) {
                if ($this.hasClass('active')) {
                    $this.next().slideUp('normal');
                    $this.removeClass("active");
                }
            } else {
                $this.closest('.accordion').find('.active').next().slideUp('normal');
                $this.closest('.accordion').find('.title').removeClass("active");
            }

            if ($this.next().is(':hidden') === true) {
                $this.next().slideDown('normal');
                $this.addClass("active");
            }
        });
        $('.accordion .contents').hide();
        $('.accordion .active').next().slideDown('normal');

        /* Twitter - open get_tweets.php and populate file with your info.
        ==================================================================================== */
        $('.tweets').each(function() {
            var $this = $(this);

            $.getJSON("twitter/get_tweets.php?get=" + $this.attr('id'), function(data) {

                if (data) {
                    var tweets = '';

                    $.each(data, function() {
                        var tweet = '<div class="tweet">';
                        tweet += '<span class="one-tweet">' + this.text + '</span>';
                        tweet += '<span class="time">' + this.time_ago + ' ago</span>';
                        tweet += '</div>';

                        tweets += tweet;
                    });

                    $this.prepend(tweets);
                }

            }).always(function() {
                //twitterCarousel();
            });

        });


        /* WOW plugin triggers animation.css on scroll
        ================================================== */
        var wow = new WOW(
          {
            boxClass:     'wow', // animated element css class (default is wow)
            offset:       250,   // distance to the element when triggering the animation (default is 0)
            mobile:       false  // trigger animations on mobile devices (true is default)
          }
        );

        /* Share Icons
        ==================================================================================== */
        $('.share').socShare({
            facebook: '.soc-fb',
            twitter: '.soc-tw',
            google_plus: '.soc-gplus',
            pinterest: '.soc-pin'
        });

       
        /* Contact Form
        ==================================================================================== */
        (function(e) {
            function n(e, n) {
                this.$form = e;
                this.indexes = {};
                this.options = t;
                for (var r in n) {
                    if (this.$form.find("#" + r).length && typeof n[r] == "function") {
                        this.indexes[r] = n[r]
                    } else {
                        this.options[r] = n[r]
                    }
                }
                this.init()
            }
            var t = {
                _error_class: "error",
                _onValidateFail: function() {}
            };
            n.prototype = {
                init: function() {
                    var e = this;
                    e.$form.on("submit", function(t) {
                        e.process();
                        if (e.hasErrors()) {
                            e.options._onValidateFail();
                            t.stopImmediatePropagation();
                            return false
                        }
                        return true
                    })
                },
                notEmpty: function(e) {
                    return e != "" ? true : false
                },
                isEmail: function(e) {
                    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e)
                },
                isUrl: function(e) {
                    var t = new RegExp("(^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|(www\\.)?))[\\w-]+(\\.[\\w-]+)+([\\w-.,@?^=%&:/~+#-]*[\\w@?^=%&;/~+#-])?", "gim");
                    return t.test(e)
                },
                elClass: "",
                setClass: function(e) {
                    this.elClass = e
                },
                process: function() {
                    this._errors = {};
                    for (var t in this.indexes) {
                        this.$el = this.$form.find("#" + t);
                        if (this.$el.length) {
                            var n = e.proxy(this.indexes[t], this, e.trim(this.$el.val()))();
                            if (this.elClass) {
                                this.elClass.toggleClass(this.options._error_class, !n);
                                this.elClass = ""
                            } else {
                                this.$el.toggleClass(this.options._error_class, !n)
                            }
                            if (!n) {
                                this._errors[t] = n
                            }
                        }
                        this.$el = null
                    }
                },
                _errors: {},
                hasErrors: function() {
                    return !e.isEmptyObject(this._errors)
                }
            };
            e.fn.isValid = function(t) {
                return this.each(function() {
                    var r = e(this);
                    if (!e.data(r, "is_valid")) {
                        e.data(r, "is_valid", new n(r, t))
                    }
                })
            }
        })(jQuery)

        var $form = $('.actionform');

        //get security question
        function setFormAutoValue(cb) {
            $.ajax({
                'url': 'action.php',
                'data': {
                    get_auto_value: ''
                },
                'type': "POST",
                'dataType': 'json',
            }).done(function(response) {

                if (typeof response.data != 'undefined') {
                    $form.find('.auto-safe label').text(unescape(response.data));
                }

                if (cb) {
                    cb();
                }

            });
        }

        $form.on('click', '.auto-refresh', function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.addClass('fa-spin');

            setFormAutoValue(function() {
                $this.removeClass('fa-spin');
            });
        });

        setFormAutoValue();

        //ajax contact form
        $form.isValid({
            'name': function(data) {
                this.setClass(this.$el.parent());
                return this.notEmpty(data);
            },
            'email': function(data) {
                this.setClass(this.$el.parent());
                return this.isEmail(data);
            },
            'subject': function(data) {
                this.setClass(this.$el.parent());
                return this.notEmpty(data);
            },
            'autovalue': function(data) {
                this.setClass(this.$el.parent());
                return this.notEmpty(data);
            }
        }).submit(function(e) {
            e.preventDefault();
            var $this = $(this);

            $this.find('.notification')
                .attr('class', 'notification');
            $this.find('.notification').text('');

            // $this.find('.loading').show();

            $.ajax({
                'url': $this.attr('action'),
                'type': $this.attr('method'),
                'dataType': 'json',
                'data': $(this).serialize()
            }).done(function(response) {
                $this.find('.loading').hide();
                if (typeof response.type != 'undefined' && typeof response.message != 'undefined') {
                    $this.find('.notification')
                        .addClass(response.type + 'msg')
                        .text(response.message);

                    if (response.type == 'success') {
                        $this.find('input[type="text"], input[type="email"], textarea').val('');
                        setFormAutoValue();
                    }
                }
            });

        });


    });

})(jQuery);
