<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for mukam themes when admin theme is not.
 *
 * 
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */

function hw_white_green_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  $form['weds_client'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки темы',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['weds_admin'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки темы (для админа)',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['weds_client']['hw_white_green_background_image_1'] = array(
    '#title' => "Главная фоновая картинка",
    '#type' => 'managed_file',
    '#required' => FALSE,
    '#description' => "Рекомендуемый размер изображения 1100 x 733",
    '#default_value' => theme_get_setting('hw_white_green_background_image_1'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // 'file_validate_image_resolution' => array('1920x1280', '1100x733'),
    ),
  );
  $form['weds_admin']['hw_white_green_background_image_2'] = array(
    '#title' => "Фоновая картинка Event",
    '#type' => 'managed_file',
    '#required' => FALSE,
    '#description' => "Рекомендуемый размер изображения 1920 x 1280",
    '#default_value' => theme_get_setting('hw_white_green_background_image_2'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // 'file_validate_image_resolution' => array('1920x1280', '1920x1280'),
    ),
  );
  $form['weds_admin']['hw_white_green_background_image_3'] = array(
    '#title' => "Фоновая картинка RSVP",
    '#type' => 'managed_file',
    '#required' => FALSE,
    '#description' => "Рекомендуемый размер изображения 1100x733",
    '#default_value' => theme_get_setting('hw_white_green_background_image_3'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // 'file_validate_image_resolution' => array('1100x733', '1100x733'),
    ),
  );

  $form['weds_admin']['image'] = array(
    '#title' => "Image",
    '#type' => 'managed_file',
    '#required' => FALSE,
    //'#description' => "1920 x 720",
    '#default_value' => theme_get_setting('image'),
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['weds_admin']['sample_path'] = array(
    '#title' => "Sample path",
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => theme_get_setting('sample_path'),
  );

  $form['#submit'][] = '_hw_white_green_form_system_theme_settings_submit';
}


function _hw_white_green_form_system_theme_settings_submit ($form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $image_settings = [
    'hw_white_green_background_image_1',
    'hw_white_green_background_image_2',
    'hw_white_green_background_image_3',
    'image',
  ];

  foreach ($image_settings as $image_setting) {
    $value = $form_state->getValue($image_setting, []);
    if ($value) {
      $fid = $value[0];
      $file = \Drupal\file\Entity\File::load($fid);
      $file->setPermanent();
      $file->save();
    }
  }
}
